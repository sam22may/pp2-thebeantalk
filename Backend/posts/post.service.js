const db = require('../database/db');

module.exports = {
    getAll,
    getAllFriends,
    getById,
    getByUserId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Post.findAll({
        include: [{
            model: db.User,
            as: 'user',
        },{
            model: db.Comment,
            as: 'comment',
        }]
    });
}

async function getAllFriends() {
    return await db.Post.findAll();
}

async function getById(id) {
    return await getPost(id);
}

async function getByUserId(id) {
    return await getUserPost(id);
}

async function create(params) {
    await db.Post.create(params);
}

async function update(id, params) {
    const post = await getPost(id);

    Object.assign(post, params);
    await post.save();
}

async function _delete(id) {
    await db.Post.destroy({
        where: {
            id: id
        }
    });
}

async function getPost(id) {
    const post = await db.Post.findByPk(id, {
        include: [{
            model: db.User,
            as: 'user',
        }]
    });
    if (!post) throw 'Post not found';
    return post;
}

async function getUserPost(id) {
    const post = await db.Post.findAll({
        where: {
            userid: id
        }
    });
    if (!post) throw 'Post not found';
    return post;
}