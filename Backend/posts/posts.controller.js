const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('middleware/validate-request');
const postService = require('./post.service');
const authorize = require('../middleware/authorize')

// routes
router.post('/post', authorize(), postSchema, post);
router.get('/', authorize(), getAll);
router.get('/:id', authorize(), getById);
router.get('/user/:userid', authorize(), getByUserId);
router.put('/:id', authorize(), updateSchema, update);
router.delete('/:id', authorize(), _delete);

module.exports = router;



function postSchema(req, res, next) {
    const schema = Joi.object({
        userid: Joi.number().required(),
        content: Joi.string().required(),
    });
    validateRequest(req, next, schema);
}

function post(req, res, next) {
    postService.create(req.body)
        .then(() => res.json({ message: 'Post successful' }))
        .catch(next);
}

function getAll(req, res, next) {
    postService.getAll()
        .then(posts => res.json(posts))
        .catch(next);
}


function getById(req, res, next) {
    postService.getById(req.params.id)
        .then(posts => res.json(posts))
        .catch(next);
}

function getByUserId(req, res, next) {
    postService.getByUserId(req.params.userid)
        .then(posts => res.json(posts))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        content: Joi.string().empty(''),
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    postService.update(req.params.id, req.body)
        .then(user => res.json(user))
        .catch(next);
}

function _delete(req, res, next) {
    postService.delete(req.params.id)
        .then(() => res.json({ message: 'Post deleted successfully' }))
        .catch(next);
}