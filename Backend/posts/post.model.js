const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        userid: { type: DataTypes.INTEGER, allowNull: false },
        content: { type: DataTypes.TEXT, allowNull: false },
    };

    const charset = {
        charset: 'utf8',
        collate: 'utf8_general_ci',
    }

    return sequelize.define('Post', attributes, charset);
}