const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('middleware/validate-request');
const commentService = require('./comment.service');
const authorize = require('../../middleware/authorize')

// routes
router.post('/post', authorize(), postSchema, post);
router.get('/', authorize(), getAll);
router.get('/:id', authorize(), getById);
router.get('/user/:userid', authorize(), getByUserId);
router.put('/:id', authorize(), updateSchema, update);
router.delete('/:id', authorize(), _delete);

module.exports = router;

function postSchema(req, res, next) {
    const schema = Joi.object({
        postid: Joi.number().required(),
        userid: Joi.number().required(),
        comment: Joi.string().required(),
        username: Joi.string().required(),
    });
    validateRequest(req, next, schema);
}

function post(req, res, next) {
    commentService.create(req.body)
        .then(() => res.json({ message: 'Post successful' }))
        .catch(next);
}

function getAll(req, res, next) {
    commentService.getAll()
        .then(posts => res.json(posts))
        .catch(next);
}


function getById(req, res, next) {
    commentService.getById(req.params.id)
        .then(posts => res.json(posts))
        .catch(next);
}

function getByUserId(req, res, next) {
    commentService.getByUserId(req.params.userid)
        .then(posts => res.json(posts))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        comment: Joi.string().empty(''),
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    commentService.update(req.params.id, req.body)
        .then(user => res.json(user))
        .catch(next);
}

function _delete(req, res, next) {
    commentService.delete(req.params.id)
        .then(() => res.json({ message: 'Post deleted successfully' }))
        .catch(next);
}