const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        userid: { type: DataTypes.INTEGER, allowNull: false },
        postid: { type: DataTypes.INTEGER, allowNull: false },
        comment: { type: DataTypes.TEXT, allowNull: false },
        username: { type: DataTypes.STRING, allowNull: false },
    };

    const charset = {
        charset: 'utf8',
        collate: 'utf8_general_ci',
    }

    return sequelize.define('Comment', attributes, charset);
}