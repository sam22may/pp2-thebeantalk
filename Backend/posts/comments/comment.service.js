const db = require('../../database/db');

module.exports = {
    getAll,
    getById,
    getByUserId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Comment.findAll({
        include: [{
            model: db.Post,
            as: 'post',
        }, {
            model: db.User,
            as: 'user',
        }]
    });
}

async function getById(id) {
    return await getPost(id);
}

async function getByUserId(id) {
    return await getUserPost(id);
}

async function create(params) {
    await db.Comment.create(params);
}

async function update(id, params) {
    const post = await getPost(id);

    Object.assign(post, params);
    await post.save();
}

async function _delete(id) {
    await db.Comment.destroy({
        where: {
            id: id
        }
    });
}

async function getPost(id) {
    const post = await db.Comment.findByPk(id, {
        include: [{
            model: db.Post,
            as: 'post',
        }, {
            model: db.User,
            as: 'user',
        }]
    });
    if (!post) throw 'Post not found';
    return post;
}

async function getUserPost(id) {
    const post = await db.Comment.findAll({
        where: {
            postid: id
        }
    });
    if (!post) throw 'Post not found';
    return post;
}