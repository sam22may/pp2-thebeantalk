const config = require('config.json');
const mysql = require('mysql2/promise');
const { Sequelize } = require('sequelize');

module.exports = db = {};



initialize();

async function initialize() {
    const { host, port, user, password, database } = config.database;
    const connection = await mysql.createConnection({ host, port, user, password });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);

    const sequelize = new Sequelize(database, user, password, { dialect: 'mysql', host: "localhost", logging: false });

    db.User = require('../users/user.model')(sequelize);
    db.Post = require('../posts/post.model')(sequelize);
    db.Meet = require('../meetup/meet.model')(sequelize);
    db.FrienshipAssociation = require('../friends/friends.model')(sequelize);
    db.Comment = require('../posts/comments/comment.model')(sequelize);

    db.User.belongsToMany(db.User, { through: 'friendshipAssociation', as: "user1", foreignKey: 'user1' })
    db.User.belongsToMany(db.User, { through: 'friendshipAssociation', as: "user2", foreignKey: 'user2' })

    db.Post.belongsTo(db.User, { as: 'user', foreignKey: 'id' });
    db.User.hasMany(db.Post, { as: 'post', foreignKey: 'userid' });

    db.Meet.belongsTo(db.User, { as: "organizer_key", foreignKey: 'organizer' })
    db.Meet.belongsTo(db.User, { as: "invited_key", foreignKey: 'invited' });

    db.Comment.belongsTo(db.Post, { as: 'post', foreignKey: 'postid' });
    db.Post.hasMany(db.Comment, { as: 'comment', foreignKey: 'postid' });
    db.Comment.belongsTo(db.User, { as: 'user', foreignKey: 'id' });
    db.User.hasMany(db.Comment, { as: 'comment', foreignKey: 'userid' });

    await sequelize.sync({ alter: true });
}

