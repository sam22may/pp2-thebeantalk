const jwt = require('express-jwt');
const { secret } = require('config.json');
const db = require('../database/db');

module.exports = authorize;

function authorize() {
    return [
        // authentifie le jwt token et l'attache à req.user
        jwt({ secret, algorithms: ['HS256'] }),

        // attache les informations de l'utilisateur a la reponse
        async (req, res, next) => {
            // recherche de l'utilisateur par la primary key (PK)
            const user = await db.User.findByPk(req.user.sub);

            // vérifie si l'utilisateur existe
            if (!user)
                return res.status(401).json({ message: 'Unauthorized' });

            req.user = user.get();
            next();
        }
    ];
}