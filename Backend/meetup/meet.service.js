const db = require('../database/db');
const { Op } = require("sequelize");
const friendService = require('../friends/friends.service');

module.exports = {
    getAllRelatedMeet,
    getAllFriendsRelatedMeet,
    create,
    update,
    delete: _delete
};

async function getAllRelatedMeet(id) {
    const allRelatedMeet = await db.Meet.findAll({
        where: {
            [Op.or] : [
                {organizer: id},
                {invited: id},
            ]
        },
        include: [
            {
               model: db.User,
                as: 'organizer_key',
                attributes: ["id", "username"],
            },
            {
                model: db.User,
                 as: 'invited_key',
                attributes: ["id", "username"],
             }
        ]
    });

    return allRelatedMeet
}

async function getAllFriendsRelatedMeet(id) {
    const friendsList = await friendService.createFriendsList(id);
    let allFriendsRelatedMeet = []
    const mainUserMeet = await searchFriendsRelatedMeet(id)
    allFriendsRelatedMeet.push(...mainUserMeet)
    for (let i = 0; i < friendsList.length; i++) {
        const friendMeet = await searchFriendsRelatedMeet(friendsList[i].id)
        friendMeet.map(meet => {
            if(!allFriendsRelatedMeet.some(friendmeet => friendmeet.id === meet.id)){
                allFriendsRelatedMeet.push(meet)
            } 
        })
    }
    return allFriendsRelatedMeet
}

async function create(params) {
    const meet = await db.Meet.findOne({
        where: {
            [Op.and] : [
                {organizer: params.organizer},
                {invited: params.invited},
                {date: params.date},
                {shopname: params.shopname},
                {address: params.address},
            ]
        }
    });
    if(meet){
        return console.log ('Meetup already exist');
    };
    await db.Meet.create(params);
}

async function update(id, params) {
    const meet = await  db.Meet.findByPk(id);
    Object.assign(meet, params);
    await meet.save();
}

async function _delete(id) {
    await db.Meet.destroy({
        where: {
            id: id
        }
    });
}

async function searchFriendsRelatedMeet(id) {
    return await db.Meet.findAll({
        where: {
            [Op.or] : [
                {organizer: id},
                {invited: id},
            ]
        },
        include: [
            {
                model: db.User,
                as: 'organizer_key',
                attributes: ["id", "username"],
            },
            {
                model: db.User,
                as: 'invited_key',
                attributes: ["id", "username"],
             }
        ]
    });
}