const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('middleware/validate-request');
const meetService = require('./meet.service');
const authorize = require('../middleware/authorize')

// routes
router.post('/post', authorize(), meetSchema, postMeet);
router.get('/related/:id', authorize(), getAllRelatedMeet);
router.get('/friendsrelated/:id', authorize(), getAllFriendsRelatedMeet);
router.put('/put/:id', authorize(), updateSchema, updateMeet);
router.delete('/delete/:id', authorize(), _delete);

module.exports = router;

function meetSchema(req, res, next) {
    const schema = Joi.object({
        organizer: Joi.number().required(),
        invited: Joi.number().required(),
        shopname: Joi.string().required(),
        address: Joi.string().required(),
        date: Joi.date().required(),
    });
    validateRequest(req, next, schema);
}

function postMeet(req, res, next) {
    meetService.create(req.body)
        .then(() => res.json({ message: 'Meetup was successfully created' }))
        .catch(next);
}

function getAllRelatedMeet(req, res, next) {
    meetService.getAllRelatedMeet(req.params.id)
        .then(meets => res.json(meets))
        .catch(next);
}

function getAllFriendsRelatedMeet(req, res, next) {
    meetService.getAllFriendsRelatedMeet(req.params.id)
        .then(meets => res.json(meets))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        organizer: Joi.number(),
        invited: Joi.number(),
        shopname: Joi.string().required(),
        address: Joi.string().required(),
        date: Joi.date(),
    });
    validateRequest(req, next, schema);
}

function updateMeet(req, res, next) {
    meetService.update(req.params.id, req.body)
        .then(user => res.json(user))
        .catch(next);
}

function _delete(req, res, next) {
    meetService.delete(req.params.id)
        .then(() => res.json({ message: 'Meetup deleted successfully' }))
        .catch(next);
}