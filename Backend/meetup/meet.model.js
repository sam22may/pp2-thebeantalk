const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        organizer: { type: DataTypes.INTEGER, allowNull: false },
        invited: { type: DataTypes.INTEGER, allowNull: false },
        shopname: { type: DataTypes.STRING, allowNull: false },
        address: { type: DataTypes.STRING, allowNull: false },
        date: { type: DataTypes.DATE, allowNull: false },
    };

    const charset = {
        charset: 'utf8',
        collate: 'utf8_general_ci',
    }

    return sequelize.define('Meet', attributes, charset);
}