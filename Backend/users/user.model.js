const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        username: { type: DataTypes.STRING, allowNull: false },
        email: { type: DataTypes.STRING, allowNull: false, unique: 'email' },
        password: { type: DataTypes.STRING, allowNull: false },
        city: { type: DataTypes.STRING, allowNull: true },
        image: { type: DataTypes.STRING, allowNull: true },
        drinker_type: { type: DataTypes.STRING, allowNull: true },
        coffee_type: { type: DataTypes.STRING, allowNull: true },
        reputation_points: { type: DataTypes.INTEGER, defaultValue: 0 },
    };

    const charset = {
        charset: 'utf8',
        collate: 'utf8_general_ci',
    }

    return sequelize.define('user', attributes, charset);
}