const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../database/db');
const { Op } = require("sequelize")

module.exports = {
    authenticate,
    getAll,
    getByKeyword,
    getById,
    create,
    update,
    delete: _delete,
};

async function authenticate({ email, password }) {
    const user = await db.User.findOne({
        where: { email },
        include: [{
            model: db.User,
            as: 'user1',
        }, {
            model: db.User,
            as: 'user2',
        }, {
            model: db.Post,
            as: 'post',
        },
    ]
    });
    if (!user || !(await bcrypt.compare(password, user.password))) {
        throw 'Email or password is incorrect';
    } else {
        const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '1d' });
        return { ...omitHash(user.get()), token };
    }
}

async function getAll() {
    return await db.User.findAll({
        include: [{
            model: db.Post,
            as: 'post',
            include: [{
                model: db.Comment,
                as: 'comment',
            }]
        }]
    });
}

async function getByKeyword(keyword) {
    return await db.User.findAll({ where: { username: { [Op.like]: "%" + keyword + "%" } } });
}

async function getById(id) {
    return await getUser(id);
}

async function create(params) {
    const password = params.password;
    if (await db.User.findOne({ where: { email: params.email } })) {
        throw 'Email "' + params.username + '" is already taken';
    }
    if (params.password) {
        params.password = await bcrypt.hash(params.password, 10);
    }
    await db.User.create(params);
    params.password = password;
    return await authenticate(params);
}

async function update(id, params) {
    const user = await getUser(id);
    const usernameChanged = params.username && user.username !== params.username;
    if (usernameChanged && await db.User.findOne({ where: { username: params.username } })) {
        throw 'Username "' + params.username + '" is already taken';
    }

    if (params.password) {
        params.hash = await bcrypt.hash(params.password, 10);
    }

    Object.assign(user, params);
    await user.save();

    const token = jwt.sign({ sub: user.id }, config.secret, { expiresIn: '7d' });
    return { ...omitHash(user.get()), token };
}

async function _delete(id) {
    const user = await getUser(id);
    await user.destroy();
}

// helper functions

async function getUser(id) {
    const user = await db.User.findByPk(id, {
        include: [{
            model: db.Post,
            as: 'post',
            include: [{
                model: db.Comment,
                as: 'comment',
                include: [{
                    model: db.User,
                    as: 'user',
                    attributes: ['username', 'image']
                }]
            }]
        }]
    });
    if (!user) throw 'User not found';
    return user;
}

function omitHash(user) {
    const { hash, ...userWithoutHash } = user;
    return userWithoutHash;
}