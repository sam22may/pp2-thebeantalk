const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
        user1: { type: DataTypes.INTEGER, allowNull: false },
        user2: { type: DataTypes.INTEGER, allowNull: false },
        confirmed: { type: DataTypes.BOOLEAN},
    };

    const charset = {
        charset: 'utf8',
        collate: 'utf8_general_ci',
    }

    return sequelize.define('friendshipAssociation', attributes, charset);
}