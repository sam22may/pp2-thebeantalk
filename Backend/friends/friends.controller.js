const express = require('express');
const router = express.Router();
const authorize = require('../middleware/authorize')
const friendsService = require('./friends.service');

router.get('/all', authorize(), getAll);
router.get("/createfriendlist/:id", authorize(), createFriendsList);
router.get("/createrequestlist/:id", authorize(), createFriendsRequestList);
router.get('/friendsrequest/:id', authorize(), friendsRequest);
router.get('/friendsinvitation/:id', authorize(), friendsInvitation);
router.post('/add', authorize(), addFriend)
router.put('/accept', authorize(), acceptFriend)
router.delete('/remove/:id', authorize(), removeFriend)

module.exports = router;

function acceptFriend(req, res, next) {
    friendsService.update(req.body.id, req.body.confirmed)
        .then(() => res.json({ message: 'Friendship accepted' }))
        .catch(next);
}

function addFriend(req, res, next) {
    friendsService.addFriend(req.body)
        .then(() => res.json({ message: 'Friend added' }))
        .catch(next);
}

function getAll(req, res, next) {
    friendsService.getAll()
        .then(friends => res.json(friends))
        .catch(next);
}

function createFriendsList(req, res, next) {
    friendsService.createFriendsList(req.params.id)
        .then(friends => res.json(friends))
        .catch(next);
}

function createFriendsRequestList(req, res, next) {
    friendsService.createFriendsRequestList(req.params.id)
        .then(friends => res.json(friends))
        .catch(next);
}

function friendsRequest(req, res, next) {
    friendsService.friendsRequest(req.params.id)
        .then(friends => res.json(friends))
        .catch(next);
}

function friendsInvitation(req, res, next) {
    friendsService.friendsInvitation(req.params.id)
        .then(friends => res.json(friends))
        .catch(next);
}

function removeFriend(req, res, next) {
    friendsService.remove(req.params.id)
        .then(() => res.json({ message: 'Friend deleted successfully' }))
        .catch(next);
}