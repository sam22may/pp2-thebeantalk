const db = require('../database/db');
const { Op } = require("sequelize");
const userService = require('../users/user.service');

module.exports = {
    getAll,
    addFriend,
    createFriendsList,
    createFriendsRequestList,
    friendsRequest,
    friendsInvitation,
    update,
    remove,
    getRelatedFriends
};

async function getAll() {
    return await db.FrienshipAssociation.findAll({});
}

async function addFriend(params) {
    await db.FrienshipAssociation.create(params);
}

async function update(id, confirmed) {
    const friend = await getFriend(id);

    friend.confirmed = confirmed;
    await friend.save();
}

async function remove(id) {
    const friend = await getFriend(id);
    await friend.destroy();
}

async function getFriend(id) {
    const friend = await db.FrienshipAssociation.findByPk(id);
    if (!friend) throw 'Friendship not found';
    return friend;
}

async function getRelatedFriends(id) {
    const friends = await db.FrienshipAssociation.findAll({
        where: {
            [Op.or]: [{
                user1: id
            }, {
                user2: id
            }]
        },
        include: [
            {
                model: db.User,
                as: 'user1',
                attributes: ["id", "usernmae"]
            },
            {
                model: db.User,
                as: 'user2',
                attributes: ["id", "usernmae"]
            }
        ]
    });
    return friends;
}
async function friendsRequest(id) {
    const allRequest = await db.FrienshipAssociation.findAll({
        include: db.FrienshipAssociation.associations.User,
    });
    const userid = parseInt(id)
    const requestList = allRequest.filter(friend => friend.user1 === userid && !friend.confirmed)
    return requestList
}

async function friendsInvitation(id){
    const allRequest = await db.FrienshipAssociation.findAll({
        include: db.FrienshipAssociation.associations.User,
    });
    const userid = parseInt(id)
    const requestList = allRequest.filter(friend => friend.user2 === userid && !friend.confirmed)
    return requestList
}


async function createFriendsList(id){
    const userid = parseInt(id)
    const FrienshipAssociation = await db.FrienshipAssociation.findAll({
        include: db.FrienshipAssociation.associations.User,
    });
    const filterFriends = await FrienshipAssociation
    .filter(friend => friend.confirmed === true)
    .filter(friend => friend.user1 === userid || friend.user2 === userid)
    const friendsList = await getListInfo(filterFriends, id)
    return friendsList
}

async function createFriendsRequestList(id){
    const userid = parseInt(id)
    const FrienshipAssociation = await db.FrienshipAssociation.findAll({
        include: db.FrienshipAssociation.associations.User,
    });
    const filterFriends = await FrienshipAssociation
    .filter(friend => friend.confirmed === false)
    .filter(friend => friend.user1 === userid || friend.user2 === userid)
    const friendsList = await getListInfo(filterFriends, id)
    return friendsList
}

async function getListInfo(filterFriends, id) {
    const friendsList = []
    const userid = parseInt(id)
    await Promise.all(filterFriends.map(async(friend) => {
        if(friend.user1 === userid){
            const userInfo = await userService.getById(friend.user2)
            userInfo.friendId = friend.id
            friendsList.push(userInfo)
        } else if (friend.user2 === userid) {
            const userInfo = await userService.getById(friend.user1)
            userInfo.friendId = friend.id
            friendsList.push(userInfo)
        } else {
            return "No friends found"
        }
    }))
    return friendsList
}