Pull the project

The root directory of the project is ./pp2-thebeantalk

log into phpmyadmin

to connect your database, go to configuration ./pp2-thebeantalk/backend/config.json

insert your databse information 

Default:  

"database": {

        "host": "localhost",

        "port": 3306,

        "user": "root",

        "password": "",

        "database": "thebeantalk"

    }


open the back-end

go to ./pp2-thebeantalk/backend

command: npm start

the back-end should run on http://localhost:4000/


open front-end

go to ./pp2-thebeantalk/thebeanapp

command: npm start

the application should run on http://localhost:3000/
