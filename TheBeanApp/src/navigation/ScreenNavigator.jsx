import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { history } from '../main/helper/history';
import Home from "../pages/Home";
import PageProfil from "../pages/Profil";
import { PrivateRoute } from './PrivateRoute';
import Search from "../pages/Search"
import FluxActualite from '../pages/FluxActualite';

const ScreenNavigator = () => {
    return (
        <Router history={history}>
                <Switch>

                    <Route exact path="/">
                        <Home />
                    </Route>

                    <PrivateRoute exact path="/profil" component={PageProfil} />

                    <PrivateRoute exact path="/search" component={Search} />

                    <PrivateRoute exact path="/exploretalks" component={FluxActualite} />

                    <Redirect from="*" to="/" />

                </Switch>
        </Router>
    )
}

export default ScreenNavigator;