import React from 'react';
import { Route } from 'react-router-dom';

function PrivateRoute({ component: Component }) {
    return (
        <Route render={() => {
            const user = localStorage.getItem('user');
            if (!user || user.message) {
                window.location.replace("/")
            }

            return <Component />
        }} />
    );
}

export { PrivateRoute };