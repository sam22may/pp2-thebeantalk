import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import ScreenNavigator from './navigation/ScreenNavigator';
import { Provider } from 'react-redux';
import { store } from './main/helper/store';


function App() {
  return (
    <div>
      <div>
        <Provider store={store}>
          <Router>
            <ScreenNavigator />
          </Router>
        </Provider>
      </div>
    </div>
  );
}

export default App;
