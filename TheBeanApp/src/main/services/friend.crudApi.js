import React from 'react';
import { authHeader } from '../helper/auhtHeader';
import { userService } from "../services/user.crudApi";

export const friendService = {
    addFriend,
    createFriendsList,
    friendsList,
    findFriendsById,
    friendsRequestList,
    request,
    invitation,
    acceptFriend,
    deleteFriend,
    findFriends
};

function addFriend(params) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(params)
    };

    return fetch(`http://localhost:4000/friends/add`, requestOptions)
        .then(userService.handleResponse)
}

async function createFriendsList(friendState) {
    const user = JSON.parse(localStorage.getItem('user'));
    const response = await findFriends()
    if(response.message) {
        return response.message
    }
    const filterFriends = await response
        .filter(friend => friend.confirmed === friendState)
        .filter(friend => friend.user1 === user.id || friend.user2 === user.id)
    const friendsList = await getListInfo(filterFriends, user)

    return friendsList
}

async function findFriends() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return await fetch(`http://localhost:4000/friends/all`, requestOptions).then(userService.handleResponse);
}

async function findFriendsById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return await fetch(`http://localhost:4000/friends/all/${id}`, requestOptions).then(userService.handleResponse);
}

async function getListInfo(filterFriends, user) {
    const friendsList = []

    await Promise.all(filterFriends.map(async(friend) => {
        if(friend.user1 === user.id){
            const userInfo = await userService.getById(friend.user2)
            userInfo.friendId = friend.id
            friendsList.push(userInfo)
        } else if (friend.user2 === user.id) {
            const userInfo = await userService.getById(friend.user1)
            userInfo.friendId = friend.id
            friendsList.push(userInfo)
        } else {
            return "No friends found"
        }
    }));
    return friendsList
}

async function friendsList(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/friends/createfriendlist/${id}`, requestOptions).then(userService.handleResponse);
}

async function friendsRequestList(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    const response = await fetch(`http://localhost:4000/friends/createrequestlist/${id}`, requestOptions).then(userService.handleResponse);
    return response
}

async function request(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/friends/friendsrequest/${id}`, requestOptions).then(userService.handleResponse);
}

async function invitation(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/friends/friendsinvitation/${id}`, requestOptions).then(userService.handleResponse);
}


async function acceptFriend(id, confirmed) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({
            id,
            confirmed
        })
    };
    return fetch(`http://localhost:4000/friends/accept`, requestOptions)
        .then(userService.handleResponse)
}

async function deleteFriend(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/friends/remove/${id}`, requestOptions).then(userService.handleResponse);
}