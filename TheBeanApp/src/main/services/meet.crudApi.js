import React from 'react';
import { authHeader } from '../helper/auhtHeader';

export const meetService = {
    getAllRelatedMeet,
    getAllFriendsRelatedMeet,
    postMeet,
    update,
    delete: _delete,
};

async function getAllRelatedMeet(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/meetup/related/${id}`, requestOptions).then(handleResponse);
}

async function getAllFriendsRelatedMeet(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/meetup/friendsrelated/${id}`, requestOptions).then(handleResponse);
}

async function postMeet(meet) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(meet)
    };

    return await fetch('http://localhost:4000/meetup/post', requestOptions).then(handleResponse)

}

async function update(meet) {
    let requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(meet)
    };

    await fetch(`http://localhost:4000/meetup/put/${meet.id}`, requestOptions)
        .then(handleResponse)
}

async function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/meetup/delete/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        return data;
    })
}