import React from 'react';
import { authHeader } from '../helper/auhtHeader';


export const userService = {
    login,
    logout,
    register,
    getAll,
    getByKeyword,
    getById,
    update,
    delete: _delete,
    handleResponse,
    updateAddFriend
};

function setUserStorage(user) {
    if(user.message) {
        return (
            localStorage.removeItem('user'),
            { message: "Email/password don't match" }
        )
    }
    localStorage.setItem('user', JSON.stringify(user));
    window.location.replace("/profil")
    return user;

}

function updateStorage(user) {
    if(user.message) {
        return (
            localStorage.removeItem('user'),
            { message: "Email/password don't match" }
        )
    }
    localStorage.setItem('user', JSON.stringify(user));
    return user;
}

async function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };
    if(email && password) {
    return await fetch('http://localhost:4000/users/authenticate', requestOptions)
        .then(handleResponse)
        .then(user => setUserStorage(user))
    } else {
        return { message: "Email and password are needed to login" }
    }
}

function logout() {
    localStorage.removeItem('user');
    window.location.reload(true);
}

async function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch('http://localhost:4000/users', requestOptions).then(handleResponse);
}

async function getByKeyword(keyword) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/users/search/${keyword}`, requestOptions).then(handleResponse);
}

async function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/users/${id}`, requestOptions).then(handleResponse);
}

async function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    await fetch('http://localhost:4000/users/register', requestOptions)
        .then(handleResponse)
        .then(user => setUserStorage(user))
}

async function update(user) {
    let requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    await fetch(`http://localhost:4000/users/${user.id}`, requestOptions)
        .then(handleResponse)
        .then(user => updateStorage(user))
}

async function updateAddFriend(user) {
    let requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    await fetch(`http://localhost:4000/users/${user.id}`, requestOptions)
        .then(handleResponse)
}

async function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);

        if (response.status === 401) {
            logout();
        }

        return data;
    })
}