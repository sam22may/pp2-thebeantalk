import React from 'react';
import { authHeader } from '../helper/auhtHeader';


export const postService = {
    getAll,
    getById,
    getByUserId,
    post,
    update,
    delete: _delete,
};

async function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch('http://localhost:4000/posts', requestOptions).then(handleResponse);
}

async function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/posts/${id}`, requestOptions).then(handleResponse);
}

async function getByUserId(userid) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/posts/user/${userid}`, requestOptions).then(handleResponse);
}

async function post(post) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(post)
    };

    return await fetch('http://localhost:4000/posts/post', requestOptions).then(handleResponse)

}

async function update(post) {
    let requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(post)
    };

    await fetch(`http://localhost:4000/posts/${post.id}`, requestOptions)
        .then(handleResponse)
}

async function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return await fetch(`http://localhost:4000/posts/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        return data;
    })
}