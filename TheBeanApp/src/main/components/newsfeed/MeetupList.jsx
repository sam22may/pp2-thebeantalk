import moment from "moment";
import React, { useEffect, useState } from "react";

const MeetupList = (props) => {

  const [meetupClass, setMeetupClass] = useState("fz-14");
  const [nameOrder, setNameOrder] = useState("");

  useEffect(() => {
    setMeetup()
  }, [props])

  const setMeetup = async () => {
    await getMeetupClasses()
    await getNameOrder()
  }
  const getNameOrder = async () => {
    if (props.meet) {
      if(props.mainUser.id === props.meet.organizer_key.id) {
        setNameOrder("You & " + props.meet.invited_key.username)
      } else if (props.mainUser.id === props.meet.invited_key.id) {
        setNameOrder( props.meet.organizer_key.username + " & You" )
      } else {
        setNameOrder(props.meet.organizer_key.username + " & " + props.meet.invited_key.username)
      }
    }
  }

  const getMeetupClasses = () => {
    const now = new Date(Date.now())
    const fomatedNow = now.toISOString()
    if (props.meet.date < fomatedNow) {
      setMeetupClass("fz-14 meetup-passed")
    } else {
      setMeetupClass("fz-14")
    }
  }
  

  return (
      <div className="m-t-35">
          <p className={meetupClass} id="meetupdate"> {moment(props.meet.date).format('dddd MMM Do YYYY, h:mm a')}</p>
          <h5 className=" text-uppercase m-b-15 m-t-15 cut-text-140">{nameOrder}</h5>
          <p className="">{props.meet.shopname} <br></br>
          {props.meet.address}</p>
      </div>

  );
}

export default MeetupList;