import React, { useEffect, useState } from "react";
import { meetService } from "../../services/meet.crudApi";
import MeetupList from "./MeetupList";

const Meetup = (props) => {
  const mainUser = JSON.parse(localStorage.getItem('user'));

  const [meets, setMeets] = useState([]);
  const [refresh, setRefresh] = useState(10);

  useEffect(() => {
    getRelatedMeet()
  }, [props])

  const getRelatedMeet = async () => {
    const relatedMeet = await meetService.getAllRelatedMeet(mainUser.id)
    await setMeets(relatedMeet)
    if(meets.length === 0) {
    setRefresh(refresh + 1)}
  }

  const showMeetup = () => {
    if (meets.length > 0) {
      return (
        <div>
          {meets.map((meet) => {
            return (<MeetupList meet={meet} mainUser={mainUser} refresh={refresh} />)
          })}
        </div>
      )
    } else {
      return (
        <div className="text-center">
          <p>No meetups</p>
        </div>
      )
    }
  }

  return (
     showMeetup()
  );
}

export default Meetup;