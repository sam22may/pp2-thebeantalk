import { useEffect, useState } from 'react';
import { ReactComponent as Icon } from "../../../img/clock-icon.svg"
import { ReactComponent as Icon2 } from "../../../img/position-icon.svg"
import moment from "moment";

const MeetupFeed = (props) => {
  const mainUser = JSON.parse(localStorage.getItem('user'));

  const [meet, setMeet] = useState(props.meet);
  const [dateClass, setDateClass] = useState("text-uppercase beige");
  const [nameOrder, setNameOrder] = useState("");
  const [refresh] = useState(20);
  const [meetupDate, setMeetupDate] = useState(moment(meet.date).format('h:mm a') + " | " + moment(meet.date).format('dddd MMMM Do YYYY'));


  useEffect(() => {
    setMeet(props.meet);
    getClass()
    getNameOrder()
  }, [props, refresh])

  const getClass = () => {
    const now = new Date(Date.now())
    const fomatedNow = now.toISOString()
    if (meet.date < fomatedNow) {
      setDateClass("text-uppercase meetup-passed")
      setMeetupDate("This meetup has passed")
    } else {
      setDateClass("text-uppercase beige")
      setMeetupDate(moment(meet.date).format('h:mm a'))
    }
  }

  const getNameOrder = () => {
    if (mainUser.id === meet.organizer_key.id) {
      setNameOrder("You & " + meet.invited_key.username)
    } else if (mainUser.id === meet.invited_key.id) {
      setNameOrder( meet.organizer_key.username + " & You" )
    } else {
      setNameOrder(meet.organizer_key.username + " & " + meet.invited_key.username)
    }
  }

  return (
    <div className="grey-to-yellow meetup-feed">
      <div className="d-flex justify-content-md-between">
        <h5 className="text-uppercase ft-w-100 yellow cut-text-420">{nameOrder}</h5>
        <h5 className="text-uppercase pb-2">Meetup</h5>
      </div>
      <div className="d-flex justify-content-md-end">
        <p className={dateClass}>{moment(meet.date).format('dddd MMM Do YYYY')}</p>
      </div>
      <div className="m-t-45 row">
        <div className="col-sm-3"></div>
        <div className="col-9">
          <div className="d-flex align-items-center">
            <Icon className="mu-icons" />
            <p className="beige"><span className={dateClass}>{meetupDate}</span></p>
          </div>
          <div className="d-flex m-t-15">
            <Icon2 className="mu-icons" />
            <p className="beige">{meet.shopname}, {meet.address}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MeetupFeed;