import React, { useEffect, useState } from "react";
import { friendService } from "../../services/friend.crudApi";
import { meetService } from "../../services/meet.crudApi";
import HeaderPost from './HeaderPost';
import "../../../styles/newsfeed.css"
import { commentService } from "../../services/comment.crudApi";
import MeetupFeed from "../newsfeed/MeetupFeed"
import moment from "moment";
import bulle from "../../../img/bulle.png"

const Actualite = (props) => {
    const mainUser = JSON.parse(localStorage.getItem('user'));

    const [posts, setPosts] = useState([]);
    const [friends, setFriends] = useState([]);
    const [talkTitle, setTalkTitle] = useState("Talks");
    const [refresh, setRefresh] = useState(0);
    const [comment, setComment] = useState('');

    useEffect(() => {
        getFriends()
        
    }, [refresh, props])



    const getFriends = async () => {
        setPosts([]);
        const allfriends = await friendService.createFriendsList(true)
        const allFriendsRelatedMeet = await meetService.getAllFriendsRelatedMeet(mainUser.id)
        const allPostsFromFriends = allfriends.concat(allFriendsRelatedMeet)
        updatePost(await allPostsFromFriends);
        setFriends(allfriends);
        startAnimation()
        if (allfriends.length === 0) {
            setTalkTitle("No friends, no talks!")
        } else {
            if (allPostsFromFriends.length > 0) {
                setTalkTitle("Talks from your beanies")
            } else {
                setTalkTitle("No talks from your beanies #boring")
            }
        }
    }

    const updatePost = async (allPosts) => {
        allPosts.map(thisPost => {
            if (thisPost.post) {
                if (thisPost.post.length > 0) {
                    const friendPost = thisPost.post
                    friendPost.map(post => {
                        setPosts(posts => [...posts, post])
                        setPosts(posts => posts.sort(sortAscending))
                    })
                }
            } else if (thisPost.organizer) {
                setPosts(posts => [...posts, thisPost])
                setPosts(posts => posts.sort(sortAscending))
            }
        })
    }

    const getPostUserInfo = (id, info) => {
        for (let i = 0; i < friends.length; i++) {
            if (friends[i].id === id) {
                if (info === "city") {
                    return friends[i].city
                }
                if (info === "username") {
                    return friends[i].username
                }
                if (info === "image") {
                    return friends[i].image
                }
                if (info === "points") {
                    return friends[i].reputation_points
                }
                if (info === "emblem") {
                    if (friends[i].reputation_points > 9) {
                        if (friends[i].reputation_points > 29) {
                            if (friends[i].reputation_points > 59) {
                                return "master"
                            }
                            return "expert"
                        }
                        return "young"
                    } else {
                        return "baby"
                    }
                } else {
                    return "Not found"
                }
            }
        }
    }

    const sortAscending = (a, b) => {
        if (a.createdAt < b.createdAt)
            return 1;
        if (a.createdAt > b.createdAt)
            return -1;
        return 0;
    }

    //Start the animation on all posts
    const startAnimation = () => {
        const posts = document.querySelectorAll(".post-animation");
        for (let i = 0; i < posts.length; i++) {
            const timer = i * 250
            setTimeout(() => {
                posts[i].classList.add("post-animation-show");
            }, timer);
        }
    }

    const showCommentForm = (id) => {
        const commentForm = document.querySelector(`#comment-form-${id}`);
        commentForm.classList.toggle("show-comment-form");
    }

    const fromNowDate = (date) => {
        return moment(date).fromNow();
    }


    const getPostHeader = (thisPost) => {
        if (thisPost.content) {
            return (
                <div className="post-animation-container">
                    <div className="Actualite-meetup bg-dark-grey act-posts-container mb-4 p-35 post-animation light-hover">
                        <HeaderPost emblem={getPostUserInfo(thisPost.userid, "emblem")} points={getPostUserInfo(thisPost.userid, "points")} avatar={getPostUserInfo(thisPost.userid, "image")} username={getPostUserInfo(thisPost.userid, "username")} content={thisPost.content} date={thisPost.createdAt} city={getPostUserInfo(thisPost.userid, "city")} />
                        <div className="mt-1">
                            <div className="mb-3 d-flex justify-content-end">
                                <div className="comment-btn d-flex align-items-center">
                                    <img src={bulle} alt="bulle" className="bulle mr-2" />
                                    <p className="" onClick={() => showCommentForm(thisPost.id)}>Comments ({thisPost.comment.length})</p>
                                </div>
                            </div>
                            <div id={"comment-form-" + thisPost.id} className="d-none">
                                <hr />
                                <form className="">
                                    <input type="hidden" name="postid" value={thisPost.id} />
                                    <input type="hidden" name="userid" value={mainUser.id} />
                                    <div className="d-flex">
                                        <input
                                            className="commentInput"
                                            type="text"
                                            name="comment"
                                            placeholder="Add a comment"
                                            onChange={(e) => setComment(e.target.value)}
                                        />
                                        <div className="d-flex justify-content-end ml-2">
                                            <button onClick={() => submitComment(thisPost.id, mainUser.id, comment, mainUser.username)} type="button" className="save-btn">Send</button>
                                        </div>
                                    </div>
                                </form>

                                <div>
                                    {thisPost.comment.map((comment) => (
                                        <div className="beige bg-gray my-2">
                                            <div className="px-3 py-2">
                                                <div className="d-flex align-items-end py-1">
                                                    <p className="yellow font-weight-bold cut-text-280">{comment.username}</p>
                                                    <p className="ml-2 comment-date">{fromNowDate(comment.createdAt)}</p>
                                                </div>
                                                <p className="">{comment.comment}</p>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            )
        } else if (thisPost.organizer) {
            return (
                <div className="post-animation-container">
                    <div className="Actualite act-posts-container mb-4 post-animation light-hover">
                        <MeetupFeed meet={thisPost} setRefresh={setRefresh} />
                    </div>
                </div>
            )
        } else {
            console.log("Error found in the post", thisPost)
        }
    }
    const submitComment = (postid, userid, comment, username) => {
        if (comment === "") {
            setComment("")
        } else {
            const post = { postid, userid, comment, username }
            commentService.post(post)
            setComment('')
            setRefresh(refresh + 1)
        }
    }

    return (
        <div>
            <div>
                <h3 className="beige m-tb-50 ml-3">{talkTitle}</h3>
            </div>

            {posts.map((post) => (
                getPostHeader(post)
            ))}
        </div>
    );
}



export default Actualite;