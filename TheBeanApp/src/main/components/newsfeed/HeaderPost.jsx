import React, { useEffect } from "react";
import moment from "moment";
import avatar1 from "../../../img/avatar/EspressBean.svg"
import avatar2 from "../../../img/avatar/EspressBean2.svg"
import avatar3 from "../../../img/avatar/FancyBean.svg"
import avatar4 from "../../../img/avatar/FancyBean2.svg"
import avatar5 from "../../../img/avatar/Bol-jaune.svg"
import avatar6 from "../../../img/avatar/Bol-noir.svg"
import avatar7 from "../../../img/avatar/paperCup1.svg"
import baby from "../../../img/beanPoints/babybean.svg"
import young from "../../../img/beanPoints/youngbean.svg"
import expert from "../../../img/beanPoints/specialist.svg"
import master from "../../../img/beanPoints/masterbean.svg"

function HeaderPost(props) {
  const fromNowDate = moment(props.date).fromNow();
  const fromCity = () => {
    if (props.city === "") {
      return "";
    } else {
      return (<span><span className='beige'>from</span> {props.city}</span>)
    }
  };



  const showEmblem = () => {
    if (props.emblem === "baby") {
      return (
        <img src={baby} alt="baby bean" className="emblem-post" />
      )
    } else if (props.emblem === "young") {
      return (
        <img src={young} alt="young bean" className="emblem-post" />
      )
    } else if (props.emblem === "expert") {
      return (
        <img src={expert} alt="expert bean" className="emblem-post" />
      )
    } else if (props.emblem === "master") {
      return (
        <img src={master} alt="master bean" className="emblem-post" />
      )
    }
  };


  const showAvatar = () => {
    if (props.avatar === "avatar1") {
      return (
        <img src={avatar1} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else if (props.avatar === "avatar2") {
      return (
        <img src={avatar2} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else if (props.avatar === "avatar3") {
      return (
        <img src={avatar3} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else if (props.avatar === "avatar4") {
      return (
        <img src={avatar4} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else if (props.avatar === "avatar5") {
      return (
        <img src={avatar5} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else if (props.avatar === "avatar6") {
      return (
        <img src={avatar6} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else if (props.avatar === "avatar7") {
      return (
        <img src={avatar7} alt="avatar" className="w-avatar-post mr-3" />
      )
    } else {
      return (<div></div>)
    }
  };

  return (
    <div className="yellow">
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          {showAvatar()}
          <h5 className="cut-text-280">{props.username}</h5>
        </div>
        <div>
          {showEmblem()}
        </div>
      </div>
      <div className=" m-b-25 m-t-15">
        <p className="beige">{props.content}</p>
      </div>
      <div className="">
        <p className="fz-14">{fromNowDate} {fromCity()}</p>
      </div>
    </div>
  );
}


export default HeaderPost;