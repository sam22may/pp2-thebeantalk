import React from 'react';
import "../../styles/nav.css";
import { userService } from '../services/user.crudApi';
import Nav from 'react-bootstrap/Nav';
import beanTalkLogo from '../../img/Asset 2.svg'
import glass from '../../img/loupe.svg'
import LogoBeanTalk from './login/Logo';


const Navigation2 = () => {

  window.addEventListener('scroll', () => {
    if (document.getElementById("mobile_nav") !== null) {
      let nav = document.getElementById("mobile_nav")
      nav.classList.toggle("sticky", window.scrollY > 130);
    }
  });

  const handleLogout = () => {
    userService.logout()
  }



  return (
    <div>
      <img src={beanTalkLogo} alt="logo" className="mobile_nav p-4" />


      <div className="navbar-border">
        <div className="container-lg">
          <div className="desktop_nav justify-content-between align-items-center p-2 mt-3 m-b-80">
            <div className="logo-nav">
              <LogoBeanTalk />
            </div>
            <Nav.Link className="link nav-item" href="/exploretalks">Explore Talks</Nav.Link>
            <Nav.Link className="link nav-item" href="/search">The Beans</Nav.Link>
            <form action="/search" method="GET" className="form_search">

              <div className="d-flex column">
                <input name="keyword" className="form-control me-2 text-uppercase" type="search" placeholder="Search" aria-label="Search" ></input>
                <div className="search-glass">
                  <img src={glass} alt="glass" />
                </div>
              </div>
            </form>
            <Nav.Link className="link nav-item" href="/profil">Profile</Nav.Link>
            <Nav.Link className="link nav-item" href="/" onClick={handleLogout}>Logout</Nav.Link>
          </div>



          <div className="mobile_nav p-2">
            <div id="mobile_nav" className="d-flex justify-content-between align-items-center">

              <Nav.Link className="link" href="/exploretalks">Explore Talks</Nav.Link>
              <Nav.Link className="link" href="/search">The Beans</Nav.Link>

              <Nav.Link className="link" href="/profil">Profile</Nav.Link>
              <Nav.Link className="link" href="/" onClick={handleLogout}>Logout</Nav.Link>
            </div>
            <form action="/search" method="GET" className="form_search d-flex justify-content-center my-4">
              <div className="d-flex">
                <input name="keyword" className="form-control me-2 text-uppercase" type="search" placeholder="Search" aria-label="Search" ></input>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>
  )
}

export default Navigation2;
