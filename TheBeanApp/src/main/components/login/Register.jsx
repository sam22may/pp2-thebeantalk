import React, { useState } from 'react';
import { userService } from "../../services/user.crudApi";
import "../../../styles/form.css";
import BeanButton from '../BeanButton';

const Register = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [city, setCity] = useState('');
    const [email, setEmail] = useState('');
    const [drinker_type, setDrinker_type] = useState('');
    const [coffee_type, setCoffee_type] = useState('');
    const [image, setImage] = useState('avatar1');
    const [errormsg, setErrormsg] = useState("");
    const [animation, setAnimation] = useState("");
    const [showForm, setShowForm] = useState(false);
    const showFormBtn = () => {
        if (showForm) {
            setAnimation("box-animation-base collapse-animation")
            setTimeout(() => { setShowForm(false) }, 1000)
        } else {
            setShowForm(true)
            setAnimation("box-animation-base expand-animation")
        }
    }

    const handleSubmit = async (e) => {
        const emailreg = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/
        if (username === '' || password === '' || email === '') {
            e.preventDefault();
            setErrormsg('Please fill all the required fields(*)');
        } 
        else if( username.length >= 50) { 
            e.preventDefault();
            setErrormsg('Please enter a shorter username(*)');
        } 
        else if(username.length <= 2) { 
            e.preventDefault();
            setErrormsg('Please enter a longer username(*)');
        } 
        else if( !emailreg.test(email) ) { 
            e.preventDefault();
            setErrormsg('Please enter valid email(*)');
        } else {
            e.preventDefault();
            setErrormsg('');
            const profil = { username, password, email, city, drinker_type, coffee_type, image };
            let reg = await userService.register(profil)
            return reg
        }
    }




    return (
        <div className="box box-register">
            <div onClick={showFormBtn} className="text-right p cursor-pointer yellow fz-20 mb-4">
                First time here? Create your bean.
            </div>
            {showForm ?
                (<div className={animation}>
                    <h3 className="beige my-3 fz-responsive">Enter your selfbean infos</h3>
                    <div>
                        <span className="error">{errormsg}</span>
                    </div>
                    <div>
                        <form action="/profil" onSubmit={handleSubmit} >
                            <label className="mb-3 yellow">Name / pseudo *</label>
                            <input
                                className="mb-3 ml-3"
                                type="text"
                                value={username}
                                placeholder="Name"
                                onChange={(e) => setUsername(e.target.value)}
                                autoComplete="username"
                            />
                            <label className="mb-3 yellow">Password *</label>
                            <input className="mb-3 ml-3"
                                type="password"
                                value={password}
                                placeholder="********"
                                onChange={(e) => setPassword(e.target.value)}
                                autoComplete="current-password"
                            />


                            <label className="mb-3 yellow">Email *</label>
                            <input className="mb-3 ml-3"
                                type="text"
                                value={email}
                                placeholder="Email"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                            <label className="mb-3 yellow">City</label>
                            <input className="mb-3 ml-3"
                                type="text"
                                value={city}
                                placeholder="City"
                                onChange={(e) => setCity(e.target.value)}
                            />

                            <label className="mb-3 yellow" htmlFor="drinker_type">What kind of drinker are you?</label>
                            <select
                                name="drinker_type"
                                className="mb-3 ml-3"
                                onChange={(e) => setDrinker_type(e.target.value)}
                            >
                                <option value="">-Select-</option>
                                <option value="morning">Morning only</option>
                                <option value="allday">All Day</option>
                                <option value="alldayallnight">All Day All Night</option>
                                <option value="addict">Addict</option>
                                <option value="casual">Casual</option>
                                <option value="hangover">Hangover</option>
                            </select>

                            <label className="mb-3 yellow" htmlFor="coffee_type">Your favorite coffee is?</label>
                            <select
                                name="coffee_type"
                                className="mb-3 ml-3"
                                onChange={(e) => setCoffee_type(e.target.value)}
                            >
                                <option value="">-Select-</option>
                                <option value="Espresso">Espresso</option>
                                <option value="Latte">Latté</option>
                                <option value="Cappucino">Cappucino</option>
                                <option value="Cortado">Cortado</option>
                                <option value="Lungo">Lungo</option>
                                <option value="Americano">Americano</option>
                                <option value="Filter">Filter</option>
                                <option value="ColdBrew">Cold Brew</option>
                                <option value="Mochaccino">Mochaccino</option>
                            </select>

                            <label className="yellow" htmlFor="image">Avatar</label>
                            <select className="ml-3" name="image" id="image" onChange={(e) => setImage(e.target.value)}>
                                <option value="avatar1">-Default-</option>
                                <option value="avatar1">Yellow Cup</option>
                                <option value="avatar2">Dark Cup</option>
                                <option value="avatar3">Fancy Cup</option>
                                <option value="avatar4">Dark Fancy Cup</option>
                                <option value="avatar5">Yellow Bowl</option>
                                <option value="avatar6">Dark Bowl</option>
                                <option value="avatar7">Paper Cup</option>
                            </select>

                            <div className="py-4">
                                <BeanButton text="Register" />
                            </div>
                        </form>

                    </div>
                </div>

                ) : null}
        </div>
    )
}

export default Register;