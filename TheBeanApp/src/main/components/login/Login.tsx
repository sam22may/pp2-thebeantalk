
import { Form, Row, Col } from 'react-bootstrap';
import React, { useState } from 'react';
import { userService } from "../../services/user.crudApi"
import { ReactComponent as Logo } from '../../../img/logo.svg';
import "../../../styles/form.css";
import Register from './Register';
import BeanButton from '../BeanButton';
import LogoBeanTalk from './Logo';


const Login = () => {
    const [errormsg, setErrormsg] = useState([]);

    const [inputs, setInputs] = useState({
        email: '',
        password: ''
    })
    const { email, password } = inputs

    function handleChange(e: any) {
        const { name, value } = e.target
        setInputs(inputs => ({ ...inputs, [name]: value }))
    }

    async function handleSubmit(e: any) {
        e.preventDefault()
        const error = await userService.login(inputs.email, inputs.password)
        if (error) {
            setErrormsg(error.message)
        }
    }
    <Row>
        <Col>1 of 2</Col>
    </Row>
    return (
        <div className="box box-login beige row ">
            
            <div className="col-md-6 d-flex justify-content-center p-0">
                <LogoBeanTalk className="logo-login"/>
            </div>
            <div className="col-md-6">
                <h3 className="pad-bot-20 mb-3">Login</h3>
                <span className="error">{errormsg}</span>
                <form onSubmit={handleSubmit}>
                    <Form.Group as={Row} className="mb-3 yellow" controlId="formPlaintextEmail">
                        <Form.Label column sm="2" className="login">
                            Email
                        </Form.Label>
                        <Col sm="10">
                            <input className=" mb-3 ml-3" placeholder="yourbean@email.com" name="email" value={email} onChange={handleChange} autoComplete="email" />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} className="mb-3 yellow" controlId="formPlaintextPassword">
                        <Form.Label column sm="2" className="login">
                            Password
                        </Form.Label>
                        <Col sm="10">
                            <input className=" mb-3 ml-3" type="password" placeholder="*******" name="password" value={password} onChange={handleChange} autoComplete="current-password" />
                        </Col>
                    </Form.Group>
                    <BeanButton text="Login"/>
                </form>
                <div className="mt-3">
                    <Register />
                </div>
            </div>
        </div>

    );
}

export default Login