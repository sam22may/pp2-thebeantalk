import React, { useEffect, useState } from 'react';
import { userService } from '../../services/user.crudApi';
import { friendService } from '../../services/friend.crudApi';
import "../../../styles/search.css"
import ModalProfil from './ModalProfil';
import BeanFriendButton from '../SearchFriends/BeanFriendButton';

const SearchList = () => {
    const mainUser = JSON.parse(localStorage.getItem('user'));

    const [users, setUsers] = useState([]);
    const [keyword, setKeyword] = useState('');
    const [display, setDisplay] = useState('none');
    const [modaluser, setModaluser] = useState({});
    const [refresh, setRefresh] = useState(0);



    useEffect(() => {
        if (keyword === '') {
            let navKeyword = window.location.search.substring(1).split('=');
            if (navKeyword[1] !== undefined) {
                setKeyword(navKeyword[1].replace('+', " "));
                window.history.replaceState({}, document.title, "/" + "search");
            }
        }

        const updateUsers = async () => {
            let data = await userService.getAll();
            data = data.filter(user => user.id !== mainUser.id)
            if (keyword !== "") {
                setUsers(await data.filter(user => user.username.toLowerCase().includes(keyword.toLowerCase())));
            } else {
                setUsers(await data);
            }
        }
        updateUsers();
    }, [keyword, modaluser, refresh]);

    const updateKeyword = (e) => {
        return setKeyword(e.target.value);
    }

    async function handleSubmit(event) {
        event.preventDefault();
        const user1 = mainUser.id
        const user2 = parseInt(event.target[0].value)
        await friendService.addFriend({ user1: user1, user2: user2, confirmed: false })
        setRefresh(refresh + 1)
    }

    const openModal = async (user) => {
        setModaluser(user)
        setRefresh(refresh + 1)
        showModal()
    }

    const showModal = () => {
        if (display === 'none') {
            document.body.style.overflow = 'hidden';
            setDisplay('flex')
        } else {
            document.body.style.overflow = 'auto';
            setDisplay('none')
        }
    }

    return (
        <div className="w-100">
            <h3 className="yellow my-5">The Beans List</h3>
            <div className="search-container bg-dark-grey p-4">

                <div className="">
                    <input className="hover-yellow" onChange={updateKeyword} value={keyword} type="text" name="keyword" id="keyword" placeholder="Search a special bean" />
                </div>

                <div style={{ display: display }}>
                        <ModalProfil user={modaluser} onClose={showModal} onSubmit={handleSubmit} setPropsRefresh={setRefresh} />
                </div>

                <div className="px-5 mt-3">
                    {users.map((user) => (
                        <div className="py-2 yellow d-flex align-items-center">
                            <div
                                onClick={() => openModal(user)}
                                className="p-3 beanlisthover w-100 mr-3"
                            >
                                <div className="row align-items-center">
                                    <p className="col-5 fz-20 font-weight-bold cut-text-180">{user.username}</p>
                                    <p className="col-6 uppercase">View more infos</p>
                                </div>
                            </div>
                            <form onSubmit={handleSubmit}>
                                <input type="hidden" name="user2" value={user.id} />
                                <BeanFriendButton user={user} />
                            </form>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default SearchList