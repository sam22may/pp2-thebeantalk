import React, { useEffect, useState } from "react";
import { friendService } from "../../services/friend.crudApi";
import BeanButton from "../BeanButton";
import BeanFriendButton from "./BeanFriendButton";
import { meetService } from "../../services/meet.crudApi";
import beanImgBtn from "../../../img/bean-btn.svg";
import baby from "../../../img/beanPoints/babybean.svg"
import young from "../../../img/beanPoints/youngbean.svg"
import expert from "../../../img/beanPoints/specialist.svg"
import master from "../../../img/beanPoints/masterbean.svg"
import { userService } from "../../services/user.crudApi";

const ModalProfil = (props) => {
  const mainUser = JSON.parse(localStorage.getItem("user"));

  let CurrentUser = props.user;
  const [user, setUser] = useState(CurrentUser);
  const [display, setDisplay] = useState("d-none");
  const [meetupanim, setMeetupanim] = useState("overflow-hidden");
  const [date, setDate] = useState("");
  const [shopname, setShopname] = useState("");
  const [address, setAddress] = useState("");
  const [disabled, setDisabled] = useState(false);
  const [friends, setFriends] = useState([]);
  const [removeDisplay, setRemoveDisplay] = useState("d-none");
  const [refresh, setRefresh] = useState(0);
  const [beanName, setBeanName] = useState("Baby Bean");
  const [reputation_score, setReputation_score] = useState(mainUser.reputation_points);

  useEffect(() => {
    setUser(CurrentUser);
    setFrienship();
    if (window.innerWidth > 768) {
      setAnimation();
    }
    const removeFriendText = document.getElementById("removeFriendText");
    if (user.friendId === undefined) {
      removeFriendText.style.display = "none";
    } else {
      removeFriendText.style.display = "block";
    }
    if (user.reputation_points > 9) {
      setBeanName("Young Bean")
    }
    if (user.reputation_points > 29) {
      setBeanName("Expert Bean")
    }
    if (user.reputation_points > 59) {
      setBeanName("Master Bean")
    }
    const getUser = async () => {
      const myUser = await userService.getById(mainUser.id);
      setReputation_score(await myUser.reputation_points);
    }
    getUser();
  }, [props, user]);

  const setAnimation = () => {
    const modal = document.getElementById("profil-modal");
    const container = document.getElementById("profil-modalcontent");
    const beanbutton = document.getElementById("beanbutton");
    const username = document.getElementById("username");
    const categories = document.getElementById("categories");
    const userinfo = document.getElementById("userinfo");
    const meetupContainer = document.getElementById("meetupContainer");

    modal.addEventListener("mousemove", (e) => {
      let xAxis = (window.innerWidth / 2 - e.pageX) / 75;
      let yAxis = (window.innerHeight / 2 - e.pageY) / 75;
      modal.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg)`;

      container.style.transform = "translateZ(25px)";
      username.style.transform = "translateZ(200px)";
      categories.style.transform = "translateZ(50px)";
      userinfo.style.transform = "translateZ(100px)";
      beanbutton.style.transform = "translateZ(50px)";
      meetupContainer.style.transform = "translateZ(50px)";
    });
  };

  const friendshipState = (username) => {
    for (let i = 0; i < friends.length; i++) {
      if (friends[i].username === username) {
        return setDisabled(false);
      }
    }
    setDisabled(true);
    setDisplay("d-none");
  };

  const setFrienship = async () => {
    const list = await friendService.friendsList(props.mainUserId);
    setFriends(await list);
    friendshipState(user.username);
  };

  const openMeetup = () => {
    if (display === "d-none") {
      setDisplay("d-block");
      setMeetupanim("overflow-hidden meetup-expand");
    } else {
      setMeetupanim("overflow-hidden meetup-collapse");
      setTimeout(() => {
        setDisplay("d-none");
      }, 1000);
    }
  };

  window.onclick = function (event) {
    if (event.target.id === "closeBtn") {
      setDisplay("d-none");
      setRemoveDisplay("d-none");
      resetMeetup()
    }
  };

  const createMeet = (e) => {
    e.preventDefault();
    meetService.postMeet({
      organizer: mainUser.id,
      invited: user.id,
      shopname: shopname,
      address: address,
      date: date,
    });
    const profil = { id: mainUser.id, reputation_points: reputation_score + 25 }
    userService.update(profil)
    const createMeetModal = document.getElementById("createMeetModal");
    createMeetModal.style.display = "block";
    setTimeout(function () {
      createMeetModal.style.display = "none";
      resetMeetup()
      setRefresh(refresh + 1)
      props.setPropsRefresh(refresh + 1)
      props.onClose();
    }, 1000);
  };

  const openRemoveFriend = () => {
    if (user.friendId !== undefined) {
      if (removeDisplay === "d-none") {
        setRemoveDisplay("d-block");
      } else {
        setRemoveDisplay("d-none");
      }
    }
  };

  const closeRemoveFriend = () => {
    setRemoveDisplay("d-none");
  };

  const removeFriend = (friendid) => {
    friendService.deleteFriend(friendid);
    window.location.reload();
  };

  const resetMeetup = () => {
    const meetupDate = document.getElementById("meetupDate")
    const shopname = document.getElementById("shopname")
    const address = document.getElementById("address")
    meetupDate.value = ""
    shopname.value = ""
    address.value = ""
    setMeetupanim("overflow-hidden");
    setDisplay("d-none");
  }

  const showEmblem = () => {
    if (user.reputation_points > 9) {
      if (user.reputation_points > 29) {
        if (user.reputation_points > 59) {
          return (
            <img src={master} alt="master bean" className="emblem-post" />
          )
        }
        return (
          <img src={expert} alt="expert bean" className="emblem-post" />
        )
      }
      return (
        <img src={young} alt="young bean" className="emblem-post" />
      )
    } else {
      return (
        <img src={baby} alt="baby bean" className="emblem-post" />
      )
    }
  };

  return (
    <div className="profil-modal-container">
      <div id="profil-modal" className="profil-modal">
        <div id="createMeetModal" class="deletemodal">
          <div class="modal-content bg-light-grey">
            <p className="text-success w-100">Meetup created</p>
          </div>
        </div>
        <div id="profil-modalcontent" className="profil-modalcontent container">
          <div id="modalcard" className="uppercase row modalcard">
            <div
              className="col-12 row justify-content-between m-0"
              id="username"
            >
              <h3 className="beige m-b-25 cut-text-420">{user.username}</h3>
              <p
                id="closeBtn"
                className="btn-close yellow cursor-pointer"
                onClick={props.onClose}
              >
                &times;
              </p>
            </div>
            <div className="col-md-4 desktop_modal" id="categories">
              <div className="m-b-25 line-height">{showEmblem()}</div>
              <h4 className="yellow m-b-25 line-height">Email:</h4>
              <h4 className="yellow m-b-25 line-height">City:</h4>
              <h4 className="yellow m-b-25 line-height">Drinker type:</h4>
              <h4 className="yellow m-b-25 line-height">Favorite coffee:</h4>
            </div>

            <div className="col-md-4" id="userinfo">
              <p className="beige m-b-25 mt-4 pb-2 line-height">{beanName}</p>
              <p className="beige m-b-25 line-height">{user.email}</p>
              <p className="beige m-b-25 line-height">{user.city}</p>
              <p className="beige m-b-25 line-height">{user.drinker_type}</p>
              <p className="beige m-b-25 line-height">{user.coffee_type}</p>
            </div>
            <div className="col-md-4 d-flex flex-column align-items-center">
              <form onSubmit={props.onSubmit} id="beanbutton">
                <input type="hidden" name="user2" value={user.id} />
                <BeanFriendButton user={user} className="btn-close yellow" />
              </form>
              <div className="mt-2">
                <p
                  id="removeFriendText"
                  className="text-danger cursor-pointer"
                  onClick={openRemoveFriend}
                >
                  Remove friend
                </p>
              </div>
              <div id="deleteFriend" class={removeDisplay}>
                <div class="modal-deleteProfil-content bg-light-grey">
                  <h5 className="beige">Confirm ?</h5>
                  <div className="d-flex justify-content-between mt-3">
                    <p
                      className="text-danger p-2 cursor-pointer light-hover rounded"
                      onClick={() => removeFriend(user.friendId)}
                    >
                      Remove
                    </p>
                    <p
                      className="beige p-2 cursor-pointer light-hover rounded"
                      onClick={closeRemoveFriend}
                    >
                      Cancel
                    </p>
                  </div>
                </div>
              </div>
              <div className="mt-5">
                <button
                  disabled={disabled}
                  type="submit"
                  className="form-btn uppercase d-flex justify-content-around align-items-center"
                  onClick={openMeetup}
                >
                  <div>
                    <img src={beanImgBtn} alt="beanBtn" className="bean-btn" />
                  </div>
                  <div>Meet up with</div>
                </button>
              </div>
            </div>
          </div>

          <div className={meetupanim}>
            <div id="meetupContainer" className={display}>
              <form action="" className="mt-3" onSubmit={createMeet}>
                <fieldset>
                  <legend className="yellow h4 px-3">
                    Set the date, time and location of your meet up
                  </legend>
                  <label className="beige" htmlFor="date">
                    date
                  </label>
                  <input
                    className="mb-3"
                    name="date"
                    id="meetupDate"
                    type="datetime-local"
                    onChange={(e) => {
                      setDate(e.target.value);
                    }}
                  />
                  <label className="beige" htmlFor="shopname">
                    Shop Name
                  </label>
                  <input
                    className="mb-3"
                    name="shopname"
                    id="shopname"
                    type="text"
                    placeholder="The Bean Talk Coffee"
                    onChange={(e) => {
                      setShopname(e.target.value);
                    }}
                  />
                  <label className="beige" htmlFor="address">
                    Address
                  </label>
                  <input
                    className="mb-3"
                    name="address"
                    id="address"
                    type="text"
                    placeholder="123 MyStreet"
                    onChange={(e) => {
                      setAddress(e.target.value);
                    }}
                  />
                  <BeanButton text="confirm" id="BeanButtonConfirm" />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalProfil;
