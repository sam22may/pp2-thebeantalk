import React, { useEffect, useState } from 'react';
import beanBtn from "../../../img/bean-btn.svg";
import { friendService } from '../../services/friend.crudApi';

const BeanFriendButton = (props) => {
    
    const [user, setUser] = useState(props.user);
    const [friends, setFriends] = useState([]);
    const [friends_request, setFriends_request] = useState([]);
    const [textBtn, setTextBtn] = useState("");
    const [disable, setDisable] = useState(false);
    const [refresh, setRefresh] = useState(0);

    useEffect(() => {
        setFrienship()
        setUser(props.user)
    }, [textBtn, refresh, props])

    const getFriends = async () => {
        const list = await friendService.createFriendsList(true)
        setFriends(await list)
    }

    const getFriendsRequest = async () => {
        const list = await friendService.createFriendsList(false)
        setFriends_request(await list)
    }

    const friendshipState = (username) => {

        for (let i = 0; i < friends.length; i++) {
            if (friends[i].username === username) {
                setTextBtn("Beanies :)")
                return setDisable(true)
            }
        }
        for (let i = 0; i < friends_request.length; i++) {
            if (friends_request[i].username === username) {
                setTextBtn("Bean waiting...")
                return setDisable(true)
            }
        }
        setTextBtn("add Bean")
        setDisable(false)
    }

    const setFrienship = async () => {
        await getFriends()
        await getFriendsRequest()
        friendshipState(user.username)
    }

    const handleClick = async () => {
        await setFrienship()
        await setRefresh(refresh + 1)
    }

    return (
            <button disabled={disable} type="submit" className="form-btn uppercase d-flex justify-content-around align-items-center" onClick={handleClick}>
                <div>
                    <img src={beanBtn} alt="beanBtn" className="bean-btn" />
                </div>
                <div>
                    {textBtn}
                </div>
            </button>
    );
}

export default BeanFriendButton;