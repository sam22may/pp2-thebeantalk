import React, { useEffect, useState } from 'react';
import { userService } from '../../services/user.crudApi';
import "../../../styles/profil.css"
import "../../../styles/form.css"
import BeanButton from '../BeanButton';
import FriendsPending from './FriendsPending';
import avatar1 from "../../../img/avatar/EspressBean.svg"
import avatar2 from "../../../img/avatar/EspressBean2.svg"
import avatar3 from "../../../img/avatar/FancyBean.svg"
import avatar4 from "../../../img/avatar/FancyBean2.svg"
import avatar5 from "../../../img/avatar/Bol-jaune.svg"
import avatar6 from "../../../img/avatar/Bol-noir.svg"
import avatar7 from "../../../img/avatar/paperCup1.svg"
import baby from "../../../img/beanPoints/babybean.svg"
import young from "../../../img/beanPoints/youngbean.svg"
import expert from "../../../img/beanPoints/specialist.svg"
import master from "../../../img/beanPoints/masterbean.svg"
import { friendService } from '../../services/friend.crudApi';


const Profil = (props) => {
    const storage = localStorage.getItem('user')
    const user = JSON.parse(storage);

    const [username, setUsername] = useState(user.username);
    const [password] = useState(user.password);
    const [city, setCity] = useState(user.city);
    const [email, setEmail] = useState(user.email);
    const [drinker_type, setDrinker_type] = useState(user.drinker_type);
    const [coffee_type, setCoffee_type] = useState(user.coffee_type);
    const [image, setImage] = useState(user.image);
    const [avatar, setAvatar] = useState(image);
    const [showForm, setShowForm] = useState(false);
    const [reputation_points, setReputation_points] = useState(user.reputation_points);
    const [refresh, setRefresh] = useState(0);
    const [beanName, setBeanName] = useState("Baby Bean");
    const [beanEmblem, setBeanEmblem] = useState(baby);
    const [errormsg, setErrormsg] = useState("");
    const [numberFriends, setNumberFriends] = useState(0);


    useEffect(() => {
        const updateUser = async () => {
            const myUser = await userService.getById(user.id);
            setReputation_points(await myUser.reputation_points)
        }
        const getNumberFriends = async () => {
            const numberFriends = await friendService.createFriendsList(true)
            setNumberFriends(numberFriends.length)
        }
        getNumberFriends()
        updateUser();
        if (reputation_points > 9) {
            setBeanName("Young Bean")
            setBeanEmblem(young)
        }
        if (reputation_points > 29) {
            setBeanName("Expert Bean")
            setBeanEmblem(expert)
        }
        if (reputation_points > 59) {
            setBeanName("Master Bean")
            setBeanEmblem(master)
        }
        if (user.image === "avatar1") {
            setAvatar(avatar1)
        }
        if (user.image === "avatar2") {
            setAvatar(avatar2)
        }
        if (user.image === "avatar3") {
            setAvatar(avatar3)
        }
        if (user.image === "avatar4") {
            setAvatar(avatar4)
        }
        if (user.image === "avatar5") {
            setAvatar(avatar5)
        }
        if (user.image === "avatar6") {
            setAvatar(avatar6)
        }
        if (user.image === "avatar7") {
            setAvatar(avatar7)
        }
    }, [props.refresh, refresh, reputation_points])

    const handleSubmit = (e) => {
        if (username === '' || email === '') {
            e.preventDefault();
            setErrormsg('Please fill all the required fields(*)');
        } else {
            e.preventDefault()
            const profil = { id: user.id, username, password, email, city, drinker_type, coffee_type, image }
            userService.update(profil)
            setRefresh(refresh + 1)
            setShowForm(false)
        }
    }

    const showFormBtn = () => {
        if (showForm) {
            setShowForm(false)
            setUsername(user.username)
            setEmail(user.email)
            setCity(user.city)
            setDrinker_type(user.drinker_type)
            setCoffee_type(user.coffee_type)
            setImage(user.image)
        } else {
            setShowForm(true)

        }
    }

    const openDeleteProfile = () => {
        const deleteModal = document.getElementById("deleteProfile");
        deleteModal.style.display = "block";
    }

    const closeDeleteProfile = () => {
        const deleteModal = document.getElementById("deleteProfile");
        deleteModal.style.display = "none";
    }

    const deleteProfile = () => {
        userService.delete(user.id);
        window.location.href = "/";
    }

    const updateRefresh = () => {
        setRefresh(refresh + 1);
        props.refreshProfil()
    }

    return (
        <div className="profil-infos-container bg-dark-grey">
            <div className="d-flex align-items-center mb-4 pl-0 col">
                
                <img src={avatar} alt="avatar" className="w-avatar-profile" />
                <div className="ml-3 p-0 col">
                    <div className="">
                        <h3 className="text-left beige cut-text-140 pb-3 text-left">{username}</h3>
                            <div className="row pb-3">
                                <p className="pl-3 pr-4 text-left align-self-center beige ">{beanName}</p>
                                <img src={beanEmblem} alt="beanpointicon" className="col p-0 w-beanPoint-profile justify-content-center" />
                            </div>
                    </div>

                    <div className="row p-0">
                        <p className="text-center beige col justify-content-center">{reputation_points}</p>
                        <p className="text-center beige col justify-content-center">{numberFriends}</p>
                        <div class="w-100"></div>
                        <p className="text-center beige col p-0 justify-content-center">points</p>
                        <p className="text-center beige col p-0 justify-content-center">friends</p>
                    </div>
                </div>
            </div>
            {showForm ? (
                <div>
                    <p className="text-danger">{errormsg}</p>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label className="yellow" htmlFor="username">Name*</label>
                            <input
                                name="username"
                                type="text"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="yellow" htmlFor="email">Email*</label>
                            <input
                                name="email"
                                type="text"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="yellow" htmlFor="city">City</label>
                            <input
                                name="city"
                                type="text"
                                onChange={(e) => setCity(e.target.value)}
                                value={city}
                            />
                        </div>
                        <div className="mb-3">
                            <label className="yellow" htmlFor="drinker_type">Drinker Type</label>
                            <select
                                name="drinker_type"
                                onChange={(e) => setDrinker_type(e.target.value)}
                            >
                                <option value={drinker_type}>{drinker_type}</option>
                                <option value="Morning only">Morning only</option>
                                <option value="All Day">All Day</option>
                                <option value="All Day All Night">All Day All Night</option>
                                <option value="Addict">Addict</option>
                                <option value="Casual">Casual</option>
                                <option value="Hangover">Hangover</option>
                            </select>
                        </div>
                        <div className="mb-3">
                            <label className="yellow" htmlFor="coffee_type">Your favorite coffee</label>
                            <select
                                name="coffee_type"
                                onChange={(e) => setCoffee_type(e.target.value)}
                            >
                                <option value={coffee_type}>{coffee_type}</option>
                                <option value="Espresso">Espresso</option>
                                <option value="Latté">Latté</option>
                                <option value="Cappucino">Cappucino</option>
                                <option value="Cortado">Cortado</option>
                                <option value="Lungo">Lungo</option>
                                <option value="Americano">Americano</option>
                                <option value="Filter">Filter</option>
                                <option value="Cold Brew">Cold Brew</option>
                                <option value="Mochaccino">Mochaccino</option>
                            </select>
                        </div>

                        <div>
                            <label className="yellow" htmlFor="image">Avatar</label>
                            <select name="image" id="image" onChange={(e) => setImage(e.target.value)}>
                                <option value={image}>{image}</option>
                                <option value="avatar1">Yellow Cup</option>
                                <option value="avatar2">Dark Cup</option>
                                <option value="avatar3">Fancy Cup</option>
                                <option value="avatar4">Dark Fancy Cup</option>
                                <option value="avatar5">Yellow Bowl</option>
                                <option value="avatar6">Dark Bowl</option>
                                <option value="avatar7">Paper Cup</option>
                            </select>
                        </div>

                        <div className="mt-4 d-flex justify-content-between align-items-center">
                            <BeanButton text="Save change(s)" />
                            <div onClick={showFormBtn}>
                                <p className="uppercase yellow cursor-pointer p-2">Cancel</p>
                            </div>
                        </div>
                    </form>

                </div>
            )
                :
                <div>
                    <div className="uppercase">
                        <h4 className="yellow">Email</h4>
                        <p className="beige m-b-25">{email}</p>
                        <h4 className="yellow">City</h4>
                        <p className="beige m-b-25">{city}</p>
                        <h4 className="yellow">Drinker type</h4>
                        <p className="beige m-b-25">{drinker_type}</p>
                        <h4 className="yellow">Favorite coffee</h4>
                        <p className="beige m-b-25">{coffee_type}</p>
                    </div>
                    <div onClick={showFormBtn} className="mb-3">
                        <BeanButton text="Update" />
                    </div>
                    <div>
                        <p className="text-danger uppercase cursor-pointer" onClick={openDeleteProfile}>Delete Profile</p>
                    </div>
                    <div id="deleteProfile" class="deleteProfile">
                        <div class="modal-deleteProfil-content bg-light-grey">
                            <h5 className="text-danger">Confirm delete profile</h5>
                            <div className="d-flex justify-content-between mt-3">
                                <p className="beige p-2 cursor-pointer light-hover rounded" onClick={deleteProfile}>Delete</p>
                                <p className="beige p-2 cursor-pointer light-hover rounded" onClick={closeDeleteProfile}>Cancel</p>
                            </div>
                        </div>
                    </div>
                </div>
            }
            <div>
                <FriendsPending refreshProfil={updateRefresh} />
            </div>
        </div>
    )
}

export default Profil;