import { useEffect, useState } from "react";
import { ReactComponent as Icon } from "../../../img/clock-icon.svg";
import { ReactComponent as Icon2 } from "../../../img/position-icon.svg";
import moment from "moment";
import Dropdown from "react-bootstrap/Dropdown";
import { meetService } from "../../services/meet.crudApi";

const MeetupFeedProfil = (props) => {
  const mainUser = JSON.parse(localStorage.getItem("user"));
  const shortDate = (props.meet.date).slice(0, 16)

  const [meet, setMeet] = useState(props.meet);
  const [dateClass, setDateClass] = useState("text-uppercase beige");
  const [nameOrder, setNameOrder] = useState("");
  const [_delete, setDelete] = useState("");
  const [refresh, setRefresh] = useState(20);
  const [showEdit, setShowEdit] = useState("none");
  const [date, setDate] = useState(shortDate);
  const [shopname, setShopname] = useState(props.meet.shopname);
  const [address, setAddress] = useState(props.meet.address);

  useEffect(() => {
    setMeet(props.meet);
    getClass();
    getNameOrder();
  }, [props, refresh]);

  const getClass = () => {
    const now = new Date(Date.now());
    const fomatedNow = now.toISOString();
    if (meet.date < fomatedNow) {
      setDateClass("text-uppercase beige meetup-passed");
    } else {
      setDateClass("text-uppercase beige");
    }
  };

  const getNameOrder = () => {
    if (mainUser.id === meet.organizer_key.id) {
      setNameOrder("You & " + meet.invited_key.username)
    } else if (mainUser.id === meet.invited_key.id) {
      setNameOrder(meet.organizer_key.username + " & You")
    } else {
      setNameOrder(meet.organizer_key.username + " & " + meet.invited_key.username)
    }
  }

  const showEditMeet = async (id) => {
    let editContentId = document.getElementById(`editcontent${id}`);
    if (showEdit === "none") {
      editContentId.classList.remove("d-none");
      setShowEdit("block");
    } else {
      editContentId.classList.add("d-none");
      setShowEdit("none");
    }
  }

  const deleteMeet = async (id) => {
    const deleteModal = document.getElementById("deleteMeetupModal");
    deleteModal.style.display = "block";
    await meetService.delete(id);
    setTimeout(async function () {
      setDelete(id)
      deleteModal.style.display = "none";
    }, 1500);
    setRefresh(refresh + 2);
    await props.setRefresh(refresh);
    await props.refreshProfil()
  }

  const submitEditedMeet = async (e) => {
    e.preventDefault();
    const id = meet.id;
    const editedMeet = { id, date, shopname, address };
    await meetService.update(editedMeet);
    showEditMeet(meet.id)
    setRefresh(refresh + 1);
    props.setRefresh(refresh);
  }

  return (
    <div className="grey-to-yellow meetup-feed">
      <div className="d-flex justify-content-md-between">
        <h5 className="text-uppercase font-weight-light yellow cut-text-420">{nameOrder}</h5>
        <div className="d-flex">
          <h5 className="text-uppercase mr-3 dark-grey">Meetup</h5>
          <Dropdown>
            <Dropdown.Toggle
              variant="warning"
              id="dropdown-basic"
              className="dropdown-btn"
            ></Dropdown.Toggle>

            <Dropdown.Menu className="bg-light-grey">
              <Dropdown.Item
                className="yellow"
                onClick={() => showEditMeet(meet.id)}
              >
                Edit
              </Dropdown.Item>
              <Dropdown.Item
                className="yellow"
                onClick={() => deleteMeet(meet.id)}
              >
                Delete
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
      <div className="d-flex justify-content-end">
        <div className="pt-1">
          <p className={dateClass}>
            {moment(meet.date).format("dddd MMM Do YYYY")}
          </p>
        </div>
      </div>
      <div className="m-t-45 row">
        <div className="col-sm-3"></div>
        <div className="col-8">
          <div className="d-flex align-items-center">
            <Icon className="mu-icons" />
            <p className="beige">
              {moment(meet.date).format("h:mm a")}
            </p>
          </div>
          <div className="d-flex m-t-15">
            <Icon2 className="mu-icons" />
            <p className="beige">
              {meet.shopname}, {meet.address}
            </p>
          </div>
        </div>
      </div>
      <div className="m-b-60">

        <form id={"editcontent" + meet.id} className="d-none" onSubmit={submitEditedMeet}>
          <fieldset>
            <legend className="yellow h4 px-3">
              Set the date, time and location of your meet up
            </legend>
            <label className="beige" htmlFor="date">
              date
            </label>
            <input
              className="mb-3"
              name="date"
              type="datetime-local"
              value={date}
              onChange={(e) => {
                setDate(e.target.value);
              }}
            />
            <label className="beige" htmlFor="shopname">
              Shop Name
            </label>
            <input
              className="mb-3"
              name="shopname"
              type="text"
              placeholder="The Bean Talk Coffee"
              value={shopname}
              onChange={(e) => {
                setShopname(e.target.value);
              }}
            />
            <label className="beige" htmlFor="address">
              Address
            </label>
            <input
              className="mb-3"
              name="address"
              type="text"
              placeholder="123 MyStreet"
              value={address}
              onChange={(e) => {
                setAddress(e.target.value);
              }}
            />

            <div className="d-flex justify-content-end align-items-center my-2">
              <p
                onClick={() => showEditMeet(meet.id)}
                className="yellow cursor-pointer mr-3"
              >
                CANCEL
              </p>
              <button
                postid={meet.id}
                onClick={submitEditedMeet}
                type="submit"
                className="save-btn"
              >
                SAVE
              </button>
            </div>
          </fieldset>
        </form>
      </div>
      <div id="deleteMeetupModal" class="deletemodal">
        <div class="modal-content bg-light-grey">
          <p className="text-danger">Meetup deleted</p>
        </div>
      </div>
    </div> /**FIN container */
  );
};

export default MeetupFeedProfil;
