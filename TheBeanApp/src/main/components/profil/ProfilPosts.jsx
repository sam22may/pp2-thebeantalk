import React, { useEffect, useState } from "react";
import "../../../styles/profil.css"
import { postService } from "../../services/post.crudApi";
import { userService } from "../../services/user.crudApi";
import BeanButton from '../BeanButton';
import Friendslist from './Friendslist';
import Dropdown from "react-bootstrap/Dropdown";
import moment from 'moment';
import MeetupFeedProfil from "./MeetupFeedProfil";
import { meetService } from "../../services/meet.crudApi";



const ProfilPosts = (props) => {
    const storage = localStorage.getItem('user')
    const user = JSON.parse(storage);

    const [username] = useState(user.username);
    const [content, setContent] = useState("");
    const [userid] = useState(user.id);
    const [posts, setPosts] = useState([]);
    const [meets, setMeets] = useState([]);
    const [refreshPost, setRefreshPost] = useState(1);
    const [_delete, setDelete] = useState("");
    const [showEdit, setShowEdit] = useState("none");
    const [editContent, setEditContent] = useState("");

    const [showLeftSection, setShowLeftSection] = useState("d-block");
    const [showMiddleSection, setShowMiddleSection] = useState("d-none");
    const [showRightSection, setShowRightSection] = useState("d-none");

    const [reputation_score, setReputation_score] = useState(user.reputation_points);

    useEffect(() => {
        const getUser = async () => {
            const myUser = await userService.getById(userid);
            setReputation_score(await myUser.reputation_points);
            sortPost(await myUser.post)
        }
        const sortPost = (myUser) => {
            const sortedPost = myUser.sort((a, b) => {
                return moment(b.createdAt).unix() - moment(a.createdAt).unix();
            });
            setPosts(sortedPost);
        }
        getUser();
        getRelatedMeet();
    }, [_delete, refreshPost, props.refresh]);

    const handleSubmit = (e) => {
        if (content === "") {
            e.preventDefault();
        } else {
            e.preventDefault();
            const post = { userid, content }
            const profil = { id: userid, reputation_points: reputation_score + 10 }
            userService.update(profil)
            postService.post(post)
            setContent("");
            setRefreshPost(refreshPost + 1);
            props.refreshProfil()
        }
    }

    let btnleft = document.getElementById('btnleft');
    let btnmiddle = document.getElementById('btnmiddle');
    let btnright = document.getElementById('btnright');

const showLeftTab = (e) =>{
    btnleft.classList.add("bg-dark-grey");
     btnleft.classList.remove("bg-light-grey");
     btnmiddle.classList.remove("bg-dark-grey");
     btnmiddle.classList.add("bg-light-grey");
     btnright.classList.add("bg-light-grey");
     btnright.classList.remove("bg-dark-grey");
        setShowLeftSection("d-block");
        setShowMiddleSection("d-none");
        setShowRightSection("d-none");
}

const showMiddleTab = (e) =>{
    if(e.target.id ===  "btnmiddle") {
        btnleft.classList.add("bg-light-grey");
        btnleft.classList.remove("bg-dark-grey");
        btnmiddle.classList.remove("bg-light-grey");
        btnmiddle.classList.add("bg-dark-grey");
        btnright.classList.add("bg-light-grey");
        btnright.classList.remove("bg-dark-grey");
        setShowLeftSection("d-none");
        setShowMiddleSection("d-block");
        setShowRightSection("d-none");
    }
}

const showRightTab = (e) =>{
    if (e.target.id === "btnright") {
        btnleft.classList.remove("bg-dark-grey");
        btnleft.classList.add("bg-light-grey");
        btnmiddle.classList.remove("bg-dark-grey");
        btnmiddle.classList.add("bg-light-grey");
        btnright.classList.remove("bg-light-grey");
        btnright.classList.add("bg-dark-grey");
        setShowMiddleSection("d-none");
        setShowLeftSection("d-none");
        setShowRightSection("d-block");
    }
}

    const formatDate = (date) => {
        const fromNowDate = moment(date).fromNow();
        return fromNowDate;
    }

    const deletePost = (id) => {
        const deleteModal = document.getElementById("deleteModal");
        deleteModal.style.display = "block";
        setTimeout(function () {
            postService.delete(id);
            setDelete(id)
            deleteModal.style.display = "none";
        }, 1500);
    }

    const showEditPost = async (id) => {
        let editContentId = document.getElementById(`editcontent${id}`);
        let postContentId = document.getElementById(`postcontent${id}`);
        const post = await postService.getById(id);
        if (showEdit === "none") {
            editContentId.classList.remove("d-none");
            postContentId.style.display = "none";
            setShowEdit("block");
            setEditContent(post.content);
        } else {
            editContentId.classList.add("d-none");
            postContentId.style.display = "block";
            setShowEdit("none");
        }
    }

    const submitEditPost = (e) => {
        e.preventDefault();
        const id = e.target.getAttribute("postid")
        const editedPost = { id, content: editContent }
        postService.update(editedPost)
        showEditPost(id)
        setRefreshPost(refreshPost + 1);
    }
  
    const getRelatedMeet = async () => {
      const relatedMeet = await meetService.getAllRelatedMeet(user.id)
      await setMeets(relatedMeet)
    }
  
    const showMeetup = () => {
      if(meets.length > 0) {
        return (
          <div>
           {meets.map((meet) => {
               if(meet.organizer === user.id) {
                    return (
                        <div className="m-t-35 ">
                            <MeetupFeedProfil meet={meet} setRefresh={setRefreshPost} refreshProfil={props.refreshProfil}/>
                        </div>
                    )
                }
            })}
          </div>
        )
      } else {
        return (
          <div className="text-center">
            <h4>No meetups</h4>
          </div>
        )
      }
    }

    return (
        <div className="profil-posts-container bg-dark-grey">
            <div className="d-flex profil-posts-title text-center">
                <p id="btnleft" onClick={showLeftTab} className="bg-dark-grey beige uppercase toggle-btn-left">Your Talks</p>
                <p id="btnmiddle" onClick={showMiddleTab} className="bg-light-grey beige uppercase toggle-btn-middle" title="See who's your friend">Bean Buddies</p>
                <p id="btnright" onClick={showRightTab} className="bg-light-grey beige uppercase toggle-btn-right" title="Meeetup with bean buddies">Meetup</p>
            </div>
             
                <div id="friendContainer"  className={showMiddleSection} >
                    <div className="beige p-35">
                        <Friendslist classname={"cut-text-280"} setRefresh={props.refreshProfil} refesh={refreshPost} />
                    </div>
                </div>

                <div id="meetupContainer"  className={showRightSection} >
                    <div className="beige p-35">
                            {showMeetup()}
                    </div>
                </div>

                <div id="postContainer" className={showLeftSection}>
                    <div className="m-lr-35 m-t-35">
                        <form>
                            <textarea
                                name="content"
                                type="text"
                                value={content}
                                onChange={(e) => setContent(e.target.value)}
                                placeholder="Share a talk with your beans"
                                className="input-h m-b-15"
                            />
                            <div onClick={handleSubmit} className="float-right m-b-25">
                                <BeanButton text="Send post" />
                            </div>

                        </form>
                    </div>
                   
                    <div className="clearfix"/>

                    <div className="p-35">
                        {posts.map((post) => (
                            <div className="shadow p-3 light-hover rounded">
                                <div>
                                    <div className="d-flex justify-content-between">
                                        <div>
                                            <h5 className="yellow cut-text-280">{username}</h5>
                                            <p className="beige m-b-15 fz-14">{formatDate(post.createdAt)}</p>
                                        </div>
                                        <Dropdown>
                                            <Dropdown.Toggle variant="warning" id="dropdown-basic" className="dropdown-btn">
                                            </Dropdown.Toggle>

                                            <Dropdown.Menu className="bg-light-grey">
                                                <Dropdown.Item className="yellow" onClick={() => showEditPost(post.id)}>Edit</Dropdown.Item>
                                                <Dropdown.Item className="yellow" onClick={() => deletePost(post.id)}>Delete</Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </div>
                                    <div className="m-b-60">
                                        <div id={"postcontent" + post.id}>
                                            <p className="beige m-b-15">{post.content}</p>
                                        </div>

                                        <form id={"editcontent" + post.id} className="d-none">
                                            <input
                                                type="textarea"
                                                value={editContent}
                                                onChange={(e) => setEditContent(e.target.value)}
                                            />
                                            <div className="d-flex justify-content-end align-items-center my-2">
                                                <p onClick={() => showEditPost(post.id)} className="yellow cursor-pointer mr-3">CANCEL</p>
                                                <button postid={post.id} onClick={submitEditPost} type="submit" className="save-btn">SAVE</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="clearfix" />
                            </div>
                        ))}
                    </div>
                </div>
                
                <div id="deleteModal" class="deletemodal">
                    <div class="modal-content bg-light-grey">
                        <p className="text-danger">Deleting talk...</p>
                    </div>
                </div>
        </div> /**FIN container */
    );
}

export default ProfilPosts;
