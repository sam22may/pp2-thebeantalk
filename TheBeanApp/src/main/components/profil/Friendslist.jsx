import React, { useEffect, useState } from 'react';
import { friendService } from "../../services/friend.crudApi";
import ModalProfil from "../SearchFriends/ModalProfil"

const Friendslist = (props) => {
    const mainUser = JSON.parse(localStorage.getItem('user'));

    const [friends, setFriends] = React.useState([]);
    const [display, setDisplay] = React.useState("none");
    const [overflow, setOverflow] = React.useState("none");
    const [modaluser, setModaluser] = useState({});
    const [refresh, setRefresh] = useState(0);

    useEffect(() => {
        async function getFriends() {
            const list = await friendService.createFriendsList(true)
            setFriends(await list)
        }
        getFriends()
    }, [refresh, modaluser, props])

    const openModal = async (user) => {
        setModaluser(user)
        setRefresh(refresh + 1)
        showModal()
    }
    const showModal = () => {
        if (display === 'none') {
            document.body.style.overflow = 'hidden';
            setDisplay('flex')
            setOverflow('hidden')
        } else {
            document.body.style.overflow = 'auto';
            setDisplay('none')
            setOverflow('scroll')
        }
    }
    
    async function handleSubmit(event) {
        event.preventDefault();
        const user1 = mainUser.id
        const user2 = parseInt(event.target[0].value)
        await friendService.addFriend({ user1: user1, user2: user2, confirmed: false })
        setRefresh(refresh + 1)
    }

    const getFriendsScreen = () => {
        if(friends.length > 0) {
            return(
                friends.map((friend) => (
                        <div onClick={() => openModal(friend)} className="cursor-pointer p-3 rounded hover-orange">
                            <p className={props.classname}>{friend.username}</p>
                        </div>
                ))
            )
        } else {
            return (<p className="beige">No friends found, come on! #ForeverAlone</p>)
        }
    }

    const updateRefresh=()=>{
        props.updateRefresh()
    }

    return (
        <div>
            <div style={{ display: display, overflow: overflow }}>
                <ModalProfil updateRefresh={updateRefresh} mainUserId={mainUser.id} user={modaluser} onClose={showModal} onSubmit={handleSubmit} setPropsRefresh={props.setRefresh} />
            </div>

            {getFriendsScreen()}
        </div>
    )
}

export default Friendslist;