import React, { useEffect, useState } from 'react';
import { friendService } from "../../services/friend.crudApi";
import { userService } from '../../services/user.crudApi';
import BeanButton from '../BeanButton';

const FriendsPending = (props) => {
    const storage = localStorage.getItem('user')
    const user = JSON.parse(storage);

    const [friends, setFriends] = useState([]);
    const [friends_invitation, setFriends_invitation] = useState([]);
    const [friends_request, setFriends_request] = useState([]);
    const [reload, setReload] = useState(0);


    useEffect(() => {
        const getFriends = async () => {
            const list = await friendService.createFriendsList(false)
            setFriends(await list)
        }
        getFriends()
        const getFriendsAssociations = async () => {
            const requestList = await friendService.request(user.id)
            setFriends_request(await requestList)
            const invitationList = await friendService.invitation(user.id)
            setFriends_invitation(await invitationList)
        }
        getFriendsAssociations()

    }, [reload])

    const handleAddFriend = async (event) => {
        event.preventDefault()
        const id = event.target[0].value
        const friendId = event.target[0].getAttribute("friendId")

        await friendService.acceptFriend(id, true)
        await updateReputationPoints(friendId)
        setReload(reload + 1)
        props.refreshProfil()
    }

    const updateReputationPoints = async (friendId) => {
        const friend = await userService.getById(friendId)
        const myUser = await userService.getById(user.id)
        const friendProfil = { id: friendId, reputation_points: friend.reputation_points + 2 }
        const myProfil = { id: user.id, reputation_points: myUser.reputation_points + 2 }
        userService.updateAddFriend(friendProfil)
        userService.updateAddFriend(myProfil)
    }

    const handleRemoveFriend = (event) => {
        event.preventDefault()
        const id = event.target[0].value
        friendService.deleteFriend(id)
        setReload(reload + 1)
    }

    const getFriendsInvit = () => {

        if (friends.length > 0) {
            let newFriendInvitation = []
            for (let i = 0; i < friends_invitation.length; i++) {
                for (let j = 0; j < friends.length; j++) {
                    if (friends[j].id === friends_invitation[i].user1) {
                        newFriendInvitation.push(friends[j])
                    }
                }
            }

            if (newFriendInvitation.length > 0) {
                return (
                    newFriendInvitation.map((friend) => (
                        <div className="d-flex justify-content-between align-items-center py-2 light-hover">
                            <p className="beige cut-text-140">{friend.username}</p>

                            <form onSubmit={handleAddFriend}>
                                <input type="hidden" name="friend" friendId={friend.id} value={friend.friendId} />
                                <BeanButton text="Accept Bean" />
                            </form>
                            <form onSubmit={handleRemoveFriend}>
                                <input type="hidden" name="friend" value={friend.friendId} />
                                <button title="cancel" className="btn btn-dark text-danger" type="submit" >X</button>
                            </form>
                        </div>
                    ))
                )
            } else {
                return (<p className="beige">No Invitations sorry!</p>)
            }
        } else {
            return (<p className="beige">No Invitations sorry!</p>)
        }
    }

    const getFriendsRequest = () => {
        if (friends.length > 0) {
            let newFriendRequest = []
            for (let i = 0; i < friends_request.length; i++) {
                for (let j = 0; j < friends.length; j++) {
                    if (friends[j].id === friends_request[i].user2) {
                        newFriendRequest.push(friends[j])
                    }
                }
            }
            if (newFriendRequest.length > 0) {
                return (
                    newFriendRequest.map((friend) => (
                        <div className="d-flex justify-content-between align-items-center py-2 light-hover">
                            <p className="beige cut-text-140">{friend.username}</p>
                            <form onSubmit={handleRemoveFriend}>
                                <input type="hidden" name="friend" value={friend.friendId} />
                                <button title="cancel" className="btn btn-dark text-danger" type="submit" >X</button>
                            </form>
                        </div>
                    ))
                )
            } else {
                return (<p className="beige">No Pending Request</p>)
            }
        } else {
            return (<p className="beige">No Pending Request</p>)
        }
    }

    return (
        <div>
            <h3 className="beige mb-3 mt-5 text-center">Invitations</h3>

            {getFriendsInvit()}

            <h3 className="beige mb-3 mt-5 text-center">Friendship pending</h3>

            {getFriendsRequest()}

        </div>
    )
}

export default FriendsPending;