import React from "react";
import beanBtn from "../../img/bean-btn.svg";

const BeanButton = (props) => {
    return (
        <button disabled={props.disabled} type="submit" className="form-btn uppercase d-flex justify-content-around align-items-center">
            <div>
                <img src={beanBtn} alt="beanBtn" className="bean-btn" />
            </div>
            <div>
                {props.text}
            </div>
        </button>
    );
}

export default BeanButton;