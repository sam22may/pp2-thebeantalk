import React from "react";
import "../../styles/global.css";
import { ReactComponent as Logo } from '../../img/logoPetit.svg';


const Footer = () => {

    return (
        <div className="footer d-flex">
            <div className="logo-footer">
                <Logo  />
            </div>

            <div className="footer-text">
                <p className="m-t-45 beige">All rights reseved © The Bean Team</p>
            </div>
        </div>
    );
}
  export default Footer;
