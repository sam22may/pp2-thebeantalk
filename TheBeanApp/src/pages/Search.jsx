import React, { useState } from "react";
import Navigation from '../main/components/Navigation';
import Friendslist from '../main/components/profil/Friendslist';
import SearchList from '../main/components/SearchFriends/SearchList';
import Footer from '../main/components/Footer';
import Meetup from "../main/components/newsfeed/Meetup";

const Search = () => {

    const [refresh, setRefresh] = useState(0);

    return (
        <div>
            <div>
                <Navigation />
            </div>
            <div className="container-lg container-100vh">
                <div className="row">

                    <div className="col-md-8">
                        <SearchList />
                    </div>
                    <div className="col-md-3">
                        <h5 className="yellow m-t-85 m-b-25 ml-4 text-uppercase">Beanies</h5>
                        <div className="grey-to-lightgrey friend-container mx-3">
                            <div className="scroller scroll4">
                                <Friendslist classname={"beige cut-text-180"} setRefresh={setRefresh} />
                            </div>
                        </div>

                        <h5 className="yellow m-t-85 m-b-25 ml-4 text-uppercase">Meetup</h5>
                        <div className="col-mx-3 ml-3 meetup grey-to-yellow">
                            <div className="scroller scroll4 scroller-meetup">
                                <Meetup refresh={refresh} />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div >
                <Footer />
            </div>
        </div>
    )
}

export default Search