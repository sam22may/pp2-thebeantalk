import React from 'react';
import Login from '../main/components/login/Login';
import Footer from "../main/components/Footer";

const Home = () => {
    localStorage.removeItem('user');

    return (
        <div>
            <div className="container-lg vhLogin mobile_margin p-t-150">
                <Login />
            </div>

            <div>
                <Footer />
            </div>
        </div>
    )
}

export default Home