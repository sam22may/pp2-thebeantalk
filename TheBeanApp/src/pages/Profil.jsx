import React, { useEffect, useState } from 'react';
import Navigation from '../main/components/Navigation';
import ProfilInfos from '../main/components/profil/ProfilInfos';
import ProfilPosts from '../main/components/profil/ProfilPosts';
import Footer from '../main/components/Footer';
import { userService } from '../main/services/user.crudApi';



const PageProfil = () => {

    const storage = localStorage.getItem('user')
    const user = JSON.parse(storage);
    const [refresh, setRefresh] = useState(0);

    const [reputation_points, setReputation_points] = useState(user.reputation_points);

    useEffect(() => {
        const updateUser = async () => {
            const myUser = await userService.getById(user.id);
            setReputation_points(await myUser.reputation_points)
        }
        updateUser();
    }, [])

    const refreshProfil = () => {
        setRefresh(refresh + 1);
    }

    return (
        <div>
            <div>
                <Navigation />
            </div>
            <div className="container-lg container-100vh">
                <div className="row">
                    <div className="col-md-4 mt-3">
                        <ProfilInfos refreshProfil={refreshProfil} refresh={refresh} />
                    </div>
                    <div className="col-md-8 mt-3">
                        <ProfilPosts refreshProfil={refreshProfil} refresh={refresh}/>
                    </div>
                </div>
            </div >
            <div>
                <Footer />
            </div>
        </div>
    )
}

export default PageProfil