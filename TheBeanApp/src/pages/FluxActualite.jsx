import React, { useState } from "react";
import Actualite from "../main/components/newsfeed/Actualite";
import Navigation from "../main/components/Navigation";
import Friendslist from "../main/components/profil/Friendslist";
import Footer from "../main/components/Footer";
import Meetup from "../main/components/newsfeed/Meetup";

const FluxActualite = () => {

    const [refresh, setRefresh] = useState(0);

    return (
        <div className=" d-flex flex-column justify-content-between">
            <div >
                <Navigation />
            </div>

            <div className="container-lg container-100vh mh-100">
                <div className="row">

                    <div className="col-md-7">
                        <Actualite refresh={refresh} />
                    </div>

                    <div className="col-md-3 offset-1">
                        <h5 className="yellow m-t-85 m-b-25 ml-4 text-uppercase">Beanies</h5>

                        <div className="friend-container mx-3 grey-to-lightgrey">
                            <div className="scroller scroll4">
                                <Friendslist classname={"beige cut-text-180"} setRefresh={setRefresh} />
                            </div>
                        </div>

                        <h5 className="yellow m-t-85 m-b-25 ml-4 text-uppercase">Meetup</h5>
                        <div className="col-mx-3 ml-3 meetup grey-to-yellow">
                            <div className="scroller scroll4 scroller-meetup">
                                <Meetup refresh={refresh} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="align-items-end">
                <Footer />
            </div>
        </div>
    );
}

export default FluxActualite;